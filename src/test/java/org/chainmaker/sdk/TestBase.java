/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.chainmaker.sdk.config.NodeConfig;
import org.chainmaker.sdk.config.SdkConfig;
import org.chainmaker.sdk.utils.FileUtils;
import org.junit.Before;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestBase {

    private static String GRPC_CERT_PATH_ORG1 = "crypto-config/wx-org1.chainmaker.org/ca/ca.crt";
    private static String GRPC_TLS_KEY_PATH_ORG1 = "crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.tls.key";
    private static String GRPC_TLS_CERT_PATH_ORG1 = "crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.tls.crt";
    private static String TLS_HOST_NAME1 = "consensus1.tls.wx-org1.chainmaker.org";

    private static String GRPC_CERT_PATH_ORG2 = "crypto-config/wx-org2.chainmaker.org/ca/ca.crt";
    private static String GRPC_TLS_KEY_PATH_ORG2 = "crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.tls.key";
    private static String GRPC_TLS_CERT_PATH_ORG2 = "crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.tls.crt";
    private static String TLS_HOST_NAME2 = "consensus1.tls.wx-org2.chainmaker.org";

    private static String GRPC_CERT_PATH_ORG3 = "crypto-config/wx-org3.chainmaker.org/ca/ca.crt";
    private static String GRPC_TLS_KEY_PATH_ORG3 = "crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.tls.key";
    private static String GRPC_TLS_CERT_PATH_ORG3 = "crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.tls.crt";
    private static String TLS_HOST_NAME3 = "consensus1.tls.wx-org3.chainmaker.org";

    private static String CHAIN_ID = "chain1";
    private static String OPENSSL_PROVIDER = "openSSL";
    private static String TLS_NEGOTIATION = "TLS";

    static String ADMIN1_KEY_PATH = "crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.key";
    static String ADMIN1_CERT_PATH = "crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.crt";
    static String ADMIN2_KEY_PATH = "crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.key";
    static String ADMIN2_CERT_PATH = "crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.crt";
    static String ADMIN3_KEY_PATH = "crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.key";
    static String ADMIN3_CERT_PATH = "crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.crt";

    static String CLIENT_KEY_PATH = "crypto-config/wx-org1.chainmaker.org/user/client1/client1.sign.key";
    static String CLIENT_CERT_PATH = "crypto-config/wx-org1.chainmaker.org/user/client1/client1.sign.crt";
    static String CLIENT_CRL_PATH = "crl/client1.crl";

    static String ORG_ID1 = "wx-org1.chainmaker.org";
    static String ORG_ID2 = "wx-org2.chainmaker.org";
    static String ORG_ID3 = "wx-org3.chainmaker.org";

    static String NODE_GRPC_URL1 = "grpcs://127.0.0.1:12301";
    //    static String NODE_GRPC_URL2 = "grpcs://127.0.0.1:12302";
    //    static String NODE_GRPC_URL3 = "grpcs://127.0.0.1:12303";


    ChainClient chainClient;
    ChainManager chainManager;
    User adminUser1;
    User adminUser2;
    User adminUser3;

//    @Before
    public void init() throws SdkException {
        byte[][] tlsCaCerts = new byte[][]{FileUtils.getResourceFileBytes(GRPC_CERT_PATH_ORG1), FileUtils.getResourceFileBytes(GRPC_CERT_PATH_ORG2)};
        Node node1 = new Node();
        node1.setClientKeyBytes(FileUtils.getResourceFileBytes(GRPC_TLS_KEY_PATH_ORG1));
        node1.setClientCertBytes(FileUtils.getResourceFileBytes(GRPC_TLS_CERT_PATH_ORG1));
        node1.setTlsCertBytes(tlsCaCerts);
        node1.setHostname(TLS_HOST_NAME1);
        node1.setGrpcUrl(NODE_GRPC_URL1);
        node1.setSslProvider(OPENSSL_PROVIDER);
        node1.setNegotiationType(TLS_NEGOTIATION);

        chainManager = ChainManager.getInstance();
        chainClient = chainManager.getChainClient(CHAIN_ID);
        User clientUser = new User(ORG_ID1, FileUtils.getResourceFileBytes(CLIENT_KEY_PATH),
                FileUtils.getResourceFileBytes(CLIENT_CERT_PATH));
        if (chainClient == null) {
            //            chainClient = chainManager.createChainClient(CHAIN_ID, clientUser, new Node[]{node1, node2, node3});
            chainClient = chainManager.createChainClient(CHAIN_ID, clientUser, new Node[]{node1});
        }
        adminUser1 = new User(ORG_ID1, FileUtils.getResourceFileBytes(ADMIN1_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN1_CERT_PATH));
        adminUser2 = new User(ORG_ID2, FileUtils.getResourceFileBytes(ADMIN2_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN2_CERT_PATH));
        adminUser3 = new User(ORG_ID3, FileUtils.getResourceFileBytes(ADMIN3_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN3_CERT_PATH));
    }

    @Before
    public void initWithConfig() throws IOException, SdkException {

        Yaml yaml = new Yaml();
        InputStream in = TestBase.class.getClassLoader().getResourceAsStream("sdk_config.yml");

        SdkConfig sdkConfig;
        sdkConfig = yaml.loadAs(in, SdkConfig.class);
        in.close();
        List<Node> nodeList = new ArrayList<>();

        for (NodeConfig nodeConfig : sdkConfig.getChainClient().getNodes()) {
            List<byte[]> tlsCaCertList = new ArrayList<>();
            for (String rootPath : nodeConfig.getTrustRootPaths()){
                tlsCaCertList.add(FileUtils.getResourceFileBytes(rootPath));
            }

            byte[][] tlsCaCerts = new byte[tlsCaCertList.size()][];
            tlsCaCertList.toArray(tlsCaCerts);

            String url = "";

            if (nodeConfig.isEnableTls()){
                url = "grpcs://" + nodeConfig.getNodeAddr();
            }else {
                url = "grpc://" + nodeConfig.getNodeAddr();
            }
            Node node = new Node();
            node.setClientKeyBytes(FileUtils.getResourceFileBytes(sdkConfig.getChainClient().getUserKeyFilePath()));
            node.setClientCertBytes(FileUtils.getResourceFileBytes(sdkConfig.getChainClient().getUserCrtFilePath()));
            node.setTlsCertBytes(tlsCaCerts);
            node.setHostname(nodeConfig.getTlsHostName());
            node.setGrpcUrl(url);
            node.setSslProvider(OPENSSL_PROVIDER);
            node.setNegotiationType(TLS_NEGOTIATION);
            node.setConnectCount(nodeConfig.getConnCnt());
            node.setMessageSize(sdkConfig.getChainClient().getRpcClient().getMaxReceiveMessageSize());
            nodeList.add(node);
        }

        Node[] nodes = new Node[nodeList.size()];
        nodeList.toArray(nodes);

        chainManager = ChainManager.getInstance();
        chainClient = chainManager.getChainClient(sdkConfig.getChainClient().getChain_id());
        User clientUser = new User(sdkConfig.getChainClient().getOrgId(), FileUtils.getResourceFileBytes(sdkConfig.getChainClient().getUserKeyFilePath()),
                FileUtils.getResourceFileBytes(sdkConfig.getChainClient().getUserCrtFilePath()));
        if (chainClient == null) {
            chainClient = chainManager.createChainClient(sdkConfig.getChainClient().getChain_id(), clientUser, nodes);
        }
        adminUser1 = new User(ORG_ID1, FileUtils.getResourceFileBytes(ADMIN1_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN1_CERT_PATH));
        adminUser2 = new User(ORG_ID2, FileUtils.getResourceFileBytes(ADMIN2_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN2_CERT_PATH));
        adminUser3 = new User(ORG_ID3, FileUtils.getResourceFileBytes(ADMIN3_KEY_PATH),
                FileUtils.getResourceFileBytes(ADMIN3_CERT_PATH));
    }
}

