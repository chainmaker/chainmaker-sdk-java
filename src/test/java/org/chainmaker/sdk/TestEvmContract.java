/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.bouncycastle.util.encoders.Hex;
import org.chainmaker.pb.common.ContractOuterClass;
import org.chainmaker.sdk.utils.CryptoUtils;
import org.chainmaker.sdk.utils.FileUtils;
import org.chainmaker.sdk.utils.UtilsException;
import org.junit.Assert;
import org.junit.Test;

import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.utils.Numeric;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class TestEvmContract extends TestBase {

    private static final String EVM_CONTRACT_FILE_PATH = "token.bin";
    private static final String CONTRACT_NAME = "token";
    private static final String CONTRACT_ARGS_EVM_PARAM = "data";
    private static String ADDRESS = "";

    private void makeAddrFromCert() {
        try {
            ADDRESS = CryptoUtils.makeAddrFromCert(chainClient.getClientUser().getCertificate());
        } catch (UtilsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCreateEvmContract() {

        //address根据用户证书生成
        makeAddrFromCert();
        //创建合约构造参数扽RLP编码值
        Function function = new Function( "" , Arrays.asList(new Address(ADDRESS)),
                Collections.emptyList());
        String methodDataStr = FunctionEncoder.encode(function);

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(CONTRACT_ARGS_EVM_PARAM, methodDataStr.substring(10));


        ResponseInfo responseInfo = null;
        try {
            byte[] byteCode = FileUtils.getResourceFileBytes(EVM_CONTRACT_FILE_PATH);
            // 1. create payload
            byte[] payload = chainClient.createPayloadOfContractCreation(CONTRACT_NAME,
                    "1", ContractOuterClass.RuntimeType.EVM, paramMap, Hex.decode(new String(byteCode)));

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfContractMgmt(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfContractMgmt(payloadsWithEndorsement);

            // 4. create contract
            responseInfo = chainClient.createContract(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testInvokeTransferEvmContract() {
        Map<String, String> params = new HashMap<>();
        String add = "0x684a7b71376f6a28af5447516d2448dbe7d84460";
        String toAddress ="0x06ffc656405700dd26aa1b2f2f38f55935f69661";
        BigInteger amount = BigInteger.valueOf(600);
        Function function = new Function( "transfer" , Arrays.asList(new Address(toAddress), new Uint256(amount)),
                Collections.emptyList());

        String methodDataStr = FunctionEncoder.encode(function);

        String method = methodDataStr.substring(0,10);

        params.put(CONTRACT_ARGS_EVM_PARAM, methodDataStr);

        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.invokeContract(CONTRACT_NAME, method, params, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Numeric.toBigInt(responseInfo.getSyncResultTxInfo().getTransaction().getResult().getContractResult().getResult().toByteArray()));
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testInvokeBalanceOfEvmContract() {
        Map<String, String> params = new HashMap<>();
        makeAddrFromCert();

        Function function = new Function( "balanceOf" , Arrays.asList(new Address(ADDRESS)),
                Collections.emptyList());

        String methodDataStr = FunctionEncoder.encode(function);

        String method = methodDataStr.substring(0,10);

        params.put(CONTRACT_ARGS_EVM_PARAM, methodDataStr);

        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.invokeContract(CONTRACT_NAME, method, params, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Numeric.toBigInt(responseInfo.getSyncResultTxInfo().getTransaction().getResult().getContractResult().getResult().toByteArray()));
        Assert.assertNotNull(responseInfo);
    }
}
