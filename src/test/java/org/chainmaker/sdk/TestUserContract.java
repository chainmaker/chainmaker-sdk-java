/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.chainmaker.pb.common.ContractOuterClass;
import org.chainmaker.sdk.utils.FileUtils;
import org.junit.Assert;
import org.junit.Test;

public class TestUserContract extends TestBase {
    private static final String QUERY_CONTRACT_METHOD = "query";
    private static final String INVOKE_CONTRACT_METHOD = "increase";
    private static final String CONTRACT_NAME = "counter";
    private static final String CONTRACT_FILE_PATH = "rust-fact-1.0.0.wasm";


    @Test
    public void testCreateContract() {
        ResponseInfo responseInfo = null;
        try {
            byte[] byteCode = FileUtils.getResourceFileBytes(CONTRACT_FILE_PATH);

            // 1. create payload
            byte[] payload = chainClient.createPayloadOfContractCreation(CONTRACT_NAME,
                    "1", ContractOuterClass.RuntimeType.WASMER, null, byteCode);

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfContractMgmt(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfContractMgmt(payloadsWithEndorsement);

            // 4. create contract
            responseInfo = chainClient.createContract(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testUpgradeContract() {
        ResponseInfo responseInfo = null;
        try {
            byte[] byteCode = FileUtils.getResourceFileBytes(CONTRACT_FILE_PATH);

            // 1. create payload
            byte[] payload = chainClient.createPayloadOfContractUpgrade(CONTRACT_NAME,
                    "2", ContractOuterClass.RuntimeType.WASMER, null, byteCode);

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfContractMgmt(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfContractMgmt(payloadsWithEndorsement);

            // 4. upgrade contract
            responseInfo = chainClient.upgradeContract(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testFreezeContract() {
        ResponseInfo responseInfo = null;
        try {
            // 1. create payload
            byte[] payload = chainClient.createPayloadOfContractFreeze(CONTRACT_NAME);

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfContractMgmt(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfContractMgmt(payloadsWithEndorsement);

            // 4. freeze contract
            responseInfo = chainClient.freezeContract(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testUnfreezeContract()  {
        ResponseInfo responseInfo = null;
        try {
            // 1. create payload
            byte[] payload = chainClient.createPayloadOfContractUnfreeze(CONTRACT_NAME);

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfContractMgmt(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfContractMgmt(payloadsWithEndorsement);

            // 4. unfreeze contract
            responseInfo = chainClient.unfreezeContract(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testRevokeContract() {
        ResponseInfo responseInfo = null;
        try {
            // 1. create payload
            byte[] payload = chainClient.createPayloadOfContractRevoke(CONTRACT_NAME);

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfContractMgmt(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfContractMgmt(payloadsWithEndorsement);

            // 4. revoke contract
            responseInfo = chainClient.revokeContract(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testInvokeContract() {
        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.invokeContract(CONTRACT_NAME, INVOKE_CONTRACT_METHOD, null, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testQueryContract() {
        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.queryContract(CONTRACT_NAME, QUERY_CONTRACT_METHOD, null,  10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }
}
