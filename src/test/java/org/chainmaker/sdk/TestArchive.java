/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.bouncycastle.util.encoders.Hex;
import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.store.Store;
import org.junit.Assert;
import org.junit.Test;

public class TestArchive extends TestBase {

    //数据归档
    @Test
    public void testArchive() {
        byte[] payloadBytes = chainClient.createArchiveBlockPayload(3);
        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.sendArchiveBlockRequest(payloadBytes, 1000);
        } catch (SdkException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        }
        Assert.assertNotNull(responseInfo.toString());
    }

    //归档恢复
    @Test
    public void testRestore() {
        ResponseInfo responseInfo = null;
        try {
            Store.BlockWithRWSet fullBlock = chainClient.getArchivedFullBlockByHeight(2);
            byte[] payloadBytes = chainClient.createRestoreBlockPayload(fullBlock.toByteArray());
            responseInfo = chainClient.sendRestoreBlockRequest(payloadBytes, 1000);
        } catch (SdkException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        }
        Assert.assertNotNull(responseInfo.toString());
    }

    //归档查询
    @Test
    public void testGetFromArchiveStore() {
        Store.BlockWithRWSet fullBlock = null;
        ChainmakerBlock.BlockInfo blockInfo = null;
        ChainmakerTransaction.TransactionInfo transactionInfo = null;

        try {
            fullBlock = chainClient.getFromArchiveStore(2);
            Assert.assertNotNull(fullBlock);

            fullBlock = chainClient.getArchivedFullBlockByHeight(2);
            Assert.assertNotNull(fullBlock);

            blockInfo = chainClient.getArchivedBlockByHeight(2, true);
            Assert.assertNotNull(blockInfo);

            blockInfo = chainClient.getArchivedBlockByHash(Hex.toHexString(blockInfo.getBlock().getHeader().getBlockHash().toByteArray()), false, 1000);
            Assert.assertNotNull(blockInfo);

            String txid = blockInfo.getBlock().getTxs(0).getHeader().getTxId();
            transactionInfo = chainClient.getArchivedTxByTxId(txid, 1000);
        } catch (SdkException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        }
        Assert.assertNotNull(transactionInfo);
    }

}
