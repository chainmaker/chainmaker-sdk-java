/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.chainmaker.pb.common.ResultOuterClass;
import org.chainmaker.sdk.utils.FileUtils;
import org.junit.Assert;
import org.junit.Test;

public class TestBaseCertManage extends TestBase {

    @Test
    public void testAddCert() {
        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.addCert(20000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testDeleteCert() {
        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.deleteCert(new String[]{"kdddddsss"},10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testQueryCert() {

        ResultOuterClass.CertInfos certInfos = null;
        try {
            chainClient.enableCertHash();
            certInfos = chainClient.queryCert(new String[]{ByteUtils.toHexString(chainClient.getClientUser().getCertHash())},10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(certInfos);
    }

    @Test
    public void testfreezeCerts() {
        ResponseInfo responseInfo = null;
        try {
            chainClient.enableCertHash();
            // 1. create payloadADMIN
            byte[] payload = chainClient.createPayloadOfFreezeCerts(new String[]{new String(FileUtils.getResourceFileBytes(ADMIN1_CERT_PATH))});

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfSystemContract(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfSystemContract(payloadsWithEndorsement);

            // 4. revoke contract
            responseInfo = chainClient.freezeCerts(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testUnfreezeCerts() {
        ResponseInfo responseInfo = null;
        try {
            chainClient.enableCertHash();
            // 1. create payload
            byte[] payload = chainClient.createPayloadOfUnfreezeCerts(new String[]{new String(FileUtils.getResourceFileBytes(ADMIN1_CERT_PATH))});

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfSystemContract(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfSystemContract(payloadsWithEndorsement);

            // 4. revoke contract
            responseInfo = chainClient.unfreezeCerts(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testRevokeCerts() {
        ResponseInfo responseInfo = null;
        try {
            chainClient.enableCertHash();
            // 1. create payload
            byte[] payload = chainClient.createPayloadOfRevokeCerts(new String(FileUtils.getResourceFileBytes(CLIENT_CRL_PATH)));

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfSystemContract(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfSystemContract(payloadsWithEndorsement);

            // 4. revoke contract
            responseInfo = chainClient.revokeCerts(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }
}
