/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.chainmaker.pb.common.ContractOuterClass;
import org.chainmaker.sdk.utils.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TestUserContractAsset extends TestBase {

    private static final String QUERY_CONTRACT_METHOD_BALANCE = "balance_of";
    private static final String QUERY_CONTRACT_METHOD_ADDR = "query_address";
    private static final String INVOKE_CONTRACT_METHOD = "issue_amount";
    private static final String CONTRACT_NAME = "asset";
    private static final String CONTRACT_FILE_PATH = "rust-asset-management-1.0.0.wasm";
    private static final String CONTRACT_ARGS_ISSUE_LIMIT = "issue_limit";
    private static final String CONTRACT_ARGS_TOTAL_SUPPLY = "total_supply";
    private static final String CONTRACT_ARGS_DEFAULT_AMOUNT = "1000000000";

    @Test
    public void testCreateContract() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(CONTRACT_ARGS_ISSUE_LIMIT, "1000");
        paramMap.put(CONTRACT_ARGS_TOTAL_SUPPLY, CONTRACT_ARGS_DEFAULT_AMOUNT);
        ResponseInfo responseInfo = null;
        try {
            byte[] byteCode = FileUtils.getResourceFileBytes(CONTRACT_FILE_PATH);

            // 1. create payload
            byte[] payload = chainClient.createPayloadOfContractCreation(CONTRACT_NAME,
                    "1", ContractOuterClass.RuntimeType.WASMER, paramMap, byteCode);

            // 2. create payloads with endorsement
            byte[] payloadWithEndorsement1 = adminUser1.signPayloadOfContractMgmt(payload, chainClient.isEnabledCertHash());

            // 3. merge endorsements using payloadsWithEndorsement
            byte[][] payloadsWithEndorsement = new byte[1][];
            payloadsWithEndorsement[0] = payloadWithEndorsement1;
            byte[] payloadWithEndorsement = chainClient.mergeSignedPayloadsOfContractMgmt(payloadsWithEndorsement);

            // 4. create contract
            responseInfo = chainClient.createContract(payloadWithEndorsement, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testInvokeContract() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("amount", "1000");
        paramMap.put("to", "fd8b009ec8f64ba5ab7ed0b872d993d839a2c89f7c6a6d6dfc0c055b5678f601");
        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.invokeContract(CONTRACT_NAME, INVOKE_CONTRACT_METHOD, paramMap, 10000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testQueryContractAddr() {
        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.queryContract(CONTRACT_NAME, QUERY_CONTRACT_METHOD_ADDR, null,  10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }

    @Test
    public void testQueryContractBalance() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("owner", "fd8b009ec8f64ba5ab7ed0b872d993d839a2c89f7c6a6d6dfc0c055b5678f601");
        ResponseInfo responseInfo = null;
        try {
            responseInfo = chainClient.queryContract(CONTRACT_NAME, QUERY_CONTRACT_METHOD_BALANCE, paramMap,  10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }
}
