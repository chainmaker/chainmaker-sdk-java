/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.chainmaker.pb.accesscontrol.PolicyOuterClass;
import org.chainmaker.pb.config.ChainConfigOuterClass;
import org.chainmaker.sdk.utils.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TestChainConfig extends TestBase {

    private final String ORG_ID = "wx-org1";
    private final String NODE_ID = "QmQVkTSF6aWzRSddT3rro6Ve33jhKpsHFaQoVxHKMWzhuN";

    @Test
    public void testGetChainConfig() {
        ChainConfigOuterClass.ChainConfig chainConfig = null;
        try {
            chainConfig = chainClient.getChainConfig(20000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(chainConfig.toString());
    }

    @Test
    public void testGetChainConfigByBlockHeight() {
        ChainConfigOuterClass.ChainConfig chainConfig = null;
        try {
            chainConfig = chainClient.getChainConfigByBlockHeight(0, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(chainConfig.toString());
    }

    @Test
    public void testGetChainConfigSequence() {
        long sequence = 0;
        try {
            sequence = chainClient.getChainConfigSequence(20000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotEquals(-1, sequence);
    }

    @Test
    public void testCreatePayloadOfChainConfigCoreUpdate() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigCoreUpdate(20,30, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigBlockUpdate() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigBlockUpdate(false,800, 10000, 1000000000, 5000, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigTrustRootAdd() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigTrustRootAdd(ORG_ID,"aaaaaa", 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigTrustRootUpdate() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigTrustRootUpdate(ORG_ID,"bbbbbb", 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigTrustRootDelete() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigTrustRootDelete(ORG_ID,10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigPermissionAdd() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigPermissionAdd(ORG_ID, PolicyOuterClass.Policy.getDefaultInstance(), 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigPermissionUpdate() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigPermissionUpdate(ORG_ID, PolicyOuterClass.Policy.getDefaultInstance(), 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigPermissionDelete() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigPermissionDelete(ORG_ID, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigConsensusNodeAddrAdd() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigConsensusNodeAddrAdd(ORG_ID, new String[]{NODE_ID}, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigConsensusNodeAddrUpdate() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigConsensusNodeAddrUpdate(ORG_ID, NODE_ID, NODE_ID, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigConsensusNodeAddrDelete() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigConsensusNodeAddrDelete(ORG_ID, NODE_ID, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigConsensusNodeOrgAdd() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigConsensusNodeOrgAdd(ORG_ID, new String[]{NODE_ID}, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigConsensusNodeOrgUpdate() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigConsensusNodeOrgUpdate(ORG_ID, new String[]{NODE_ID}, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreateChainConfigConsensusNodeOrgDeletePayload() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigConsensusNodeOrgDelete(ORG_ID, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigConsensusExtAdd() {
        Map<String, String> params = new HashMap<>();
        params.put("aaaa", "bbbb");
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigConsensusExtAdd(params, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigConsensusExtUpdatePayload() {
        Map<String, String> params = new HashMap<>();
        params.put("aaaa", "bbbb");
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigConsensusExtUpdate(params, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testCreatePayloadOfChainConfigConsensusExtDelete() {
        byte[] payload = new byte[0];
        try {
            payload = chainClient.createPayloadOfChainConfigConsensusExtDelete(new String[]{"aaaa"}, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(Arrays.toString(payload));
    }

    @Test
    public void testUpdateChainConfig() {
        ResponseInfo responseInfo = null;
        try {
            byte[] payload = chainClient.createPayloadOfChainConfigCoreUpdate(50, 50, 10000);
            User adminUser = new User(ORG_ID1, FileUtils.getResourceFileBytes(ADMIN1_KEY_PATH), FileUtils.getResourceFileBytes(ADMIN1_CERT_PATH));
            byte[] signedPayload = adminUser.signPayloadOfSystemContract(payload, chainClient.isEnabledCertHash());
            byte[][] signedPayloads = new byte[1][];
            signedPayloads[0] = signedPayload;
            byte[] mergedPayload = chainClient.mergeSignedPayloadsOfSystemContract(signedPayloads);
            responseInfo = chainClient.updateChainConfig(mergedPayload, 10000);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(responseInfo);
    }
}
