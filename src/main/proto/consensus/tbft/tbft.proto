/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

syntax = "proto3";

option java_package = "org.chainmaker.pb.consensus.tbft";
option go_package = "chainmaker.org/chainmaker-go/pb/protogo/consensus/tbft";
package tbft;

import "common/block.proto";
import "common/request.proto";

// ValidatorSet represents the set of validators
message ValidatorSet {
  repeated string validators = 1;
}

// TBFTMsgType defines different type message in tbft
enum TBFTMsgType {
  propose   = 0;
  prevote   = 1;
  precommit = 2;
  state     = 3;
}

message TBFTMsg {
  TBFTMsgType type = 1;
  bytes msg        = 2;
}

// Proposal defined a consesensus proposal which can 
// be gossiped to other node and can be serilized 
// for persistent store.
message Proposal {
  string voter                 = 1;
  int64 height                 = 2;
  int32 round                  = 3;
  int32 pol_round              = 4;
  common.Block block                  = 5;
  common.EndorsementEntry endorsement = 6;
}

// VoteType represents the type of vote
enum VoteType {
  VotePrevote   = 0;
  VotePrecommit = 1;
}

// Vote represents a tbft vote
message Vote {
  VoteType type = 1;
  string voter  = 2;
  int64 height  = 3;
  int32 round   = 4;
  bytes hash    = 5;
  common.EndorsementEntry endorsement = 6;
}

// BlockVotes represents votes as key-value form
message BlockVotes {
  map<string, Vote> votes = 1;
  int64 sum               = 2;
}

// VoteSet represents a set of vote at `height` and `round`
message VoteSet {
  VoteType type                          = 1;
  int64 height                           = 2;
  int32 round                            = 3;
  int64 sum                              = 4;
  bytes maj23                            = 5;
  map<string, Vote> votes               = 6;
  map<string, BlockVotes> votes_by_block = 7;
}

// RoundVoteSet represents voteSet of a `round`
message RoundVoteSet {
  int64 height       = 1;
  int32 round        = 2;
  VoteSet prevotes   = 3;
  VoteSet precommits = 4;
}

// HeightRoundVoteSet represents voteSet of a `height`
message HeightRoundVoteSet {
  int64 height                           = 1;
  int32 round                            = 2; // max round
  map<int32, RoundVoteSet> roundVoteSets = 3;
}

// Step represents the step in a round 
enum Step {
	NewHeight     = 0;
  NewRound      = 1;
	Propose       = 2;
	Prevote       = 3;
	PrevoteWait   = 4;
	Precommit     = 5;
	PrecommitWait = 6;
	Commit        = 7;
}

// ConsensusState represents the state of tbft instance
message ConsensusState {
  string id                                = 1;
  int64 height                             = 2;
  int32 round                              = 3; // max round
  Step step                                = 4; // step
  Proposal proposal                        = 5;
  Proposal verifing_proposal               = 6;
  Proposal locked_proposal                 = 7;
  Proposal valid_proposal                  = 8;
  HeightRoundVoteSet height_round_vote_set = 9;
}

// GossipState represents the state of tbft instance
message GossipState {
  string id                   = 1;
  int64 height                = 2;
  int32 round                 = 3; // max round
  Step step                   = 4; // step
  bytes proposal              = 5;
  bytes verifing_proposal     = 6;
  RoundVoteSet round_vote_set = 7;
}

// TimeoutInfo represents the timeout event
message TimeoutInfo {
    int64 duration = 1;
    int64 height = 2;
    int32 round = 3;
    Step step = 4;
}

// WalEntryType represents different types of entries in Wal
enum WalEntryType {
  TimeoutEntry  = 0;
  ProposalEntry = 1;
  VoteEntry     = 2;
}

// WalEntry represents the log entry in Wal
message WalEntry {
  int64 height            = 1;
  uint64 heightFirstIndex = 2; // first index in wal of current height
  WalEntryType type       = 3; // log entry type
  bytes data              = 4; // data of entry
}
