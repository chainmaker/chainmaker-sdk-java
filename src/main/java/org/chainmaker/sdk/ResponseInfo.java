/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.common.ChainmakerTransaction.TransactionInfo;
import org.chainmaker.pb.common.ResultOuterClass;
import org.chainmaker.pb.common.ResultOuterClass.TxResponse;

// The response of a transaction request
public class ResponseInfo {
    private ResultOuterClass.TxResponse txResponse;
    private String txId;
    private ChainmakerTransaction.TransactionInfo syncResultTxInfo;

    public TxResponse getTxResponse() {
        return txResponse;
    }

    public void setTxResponse(TxResponse txResponse) {
        this.txResponse = txResponse;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public TransactionInfo getSyncResultTxInfo() {
        return syncResultTxInfo;
    }

    public void setSyncResultTxInfo(TransactionInfo syncResultTxInfo) {
        this.syncResultTxInfo = syncResultTxInfo;
    }
}
