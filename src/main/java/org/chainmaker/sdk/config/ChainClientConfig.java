/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk.config;

public class ChainClientConfig {

    private String chain_id;

    private String org_id;

    private String user_key_file_path;

    private String user_crt_file_path;

    private String user_sign_key_file_path;

    private String user_sign_crt_file_path;

    private NodeConfig[] nodes;

    private ArchiveConfig archive;

    private RpcClientConfig rpc_client;

    public String getChain_id() {
        return chain_id;
    }

    public void setChain_id(String chain_id) {
        this.chain_id = chain_id;
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getUser_key_file_path() {
        return user_key_file_path;
    }

    public void setUser_key_file_path(String user_key_file_path) {
        this.user_key_file_path = user_key_file_path;
    }

    public String getUser_crt_file_path() {
        return user_crt_file_path;
    }

    public void setUser_crt_file_path(String user_crt_file_path) {
        this.user_crt_file_path = user_crt_file_path;
    }

    public String getUser_sign_key_file_path() {
        return user_sign_key_file_path;
    }

    public void setUser_sign_key_file_path(String user_sign_key_file_path) {
        this.user_sign_key_file_path = user_sign_key_file_path;
    }

    public String getUser_sign_crt_file_path() {
        return user_sign_crt_file_path;
    }

    public void setUser_sign_crt_file_path(String user_sign_crt_file_path) {
        this.user_sign_crt_file_path = user_sign_crt_file_path;
    }

    public NodeConfig[] getNodes() {
        return nodes;
    }

    public void setNodes(NodeConfig[] nodes) {
        this.nodes = nodes;
    }

    public ArchiveConfig getArchive() {
        return archive;
    }

    public void setArchive(ArchiveConfig archive) {
        this.archive = archive;
    }

    public String getChainId() {
        return chain_id;
    }

    public void setChainId(String chain_id) {
        this.chain_id = chain_id;
    }

    public String getOrgId() {
        return org_id;
    }

    public void setOrgId(String org_id) {
        this.org_id = org_id;
    }

    public String getUserKeyFilePath() {
        return user_key_file_path;
    }

    public void setUserKeyFilePath(String user_key_file_path) {
        this.user_key_file_path = user_key_file_path;
    }

    public String getUserCrtFilePath() {
        return user_crt_file_path;
    }

    public void setUserCrtFilePath(String user_crt_file_path) {
        this.user_crt_file_path = user_crt_file_path;
    }

    public String getUserSignKeyFilePath() {
        return user_sign_key_file_path;
    }

    public void setUserSignKeyFilePath(String user_sign_key_file_path) {
        this.user_sign_key_file_path = user_sign_key_file_path;
    }

    public String getUserSignCrtFilePath() {
        return user_sign_crt_file_path;
    }

    public void setUserSignCrtFilePath(String user_sign_crt_file_path) {
        this.user_sign_crt_file_path = user_sign_crt_file_path;
    }

    public RpcClientConfig getRpcClient() {
        return rpc_client;
    }

    public void setRpcClient(RpcClientConfig rpcClient) {
        this.rpc_client = rpcClient;
    }

    public RpcClientConfig getRpc_client() {
        return rpc_client;
    }

    public void setRpc_client(RpcClientConfig rpc_client) {
        this.rpc_client = rpc_client;
    }
}
