/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk.config;

public class SdkConfig {

    private ChainClientConfig chain_client;

    public ChainClientConfig getChain_client() {
        return chain_client;
    }

    public void setChain_client(ChainClientConfig chain_client) {
        this.chain_client = chain_client;
    }

    public ChainClientConfig getChainClient() {
        return chain_client;
    }

    public void setChainClient(ChainClientConfig chain_client) {
        this.chain_client = chain_client;
    }
}
