/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk.config;

public class NodeConfig {

    private String node_addr;

    private int conn_cnt;

    private boolean enable_tls;

    private String[] trust_root_paths;

    private String tls_host_name;

    public String getNode_addr() {
        return node_addr;
    }

    public void setNode_addr(String node_addr) {
        this.node_addr = node_addr;
    }

    public int getConn_cnt() {
        return conn_cnt;
    }

    public void setConn_cnt(int conn_cnt) {
        this.conn_cnt = conn_cnt;
    }

    public boolean isEnable_tls() {
        return enable_tls;
    }

    public void setEnable_tls(boolean enable_tls) {
        this.enable_tls = enable_tls;
    }

    public String[] getTrust_root_paths() {
        return trust_root_paths;
    }

    public void setTrust_root_paths(String[] trust_root_paths) {
        this.trust_root_paths = trust_root_paths;
    }

    public String getTls_host_name() {
        return tls_host_name;
    }

    public void setTls_host_name(String tls_host_name) {
        this.tls_host_name = tls_host_name;
    }

    public String getNodeAddr() {
        return node_addr;
    }

    public void setNodeAddr(String node_addr) {
        this.node_addr = node_addr;
    }

    public int getConnCnt() {
        return conn_cnt;
    }

    public void setConnCnt(int conn_cnt) {
        this.conn_cnt = conn_cnt;
    }

    public boolean isEnableTls() {
        return enable_tls;
    }

    public void setEnableTls(boolean enable_tls) {
        this.enable_tls = enable_tls;
    }

    public String[] getTrustRootPaths() {
        return trust_root_paths;
    }

    public void setTrustRootPaths(String[] trust_root_paths) {
        this.trust_root_paths = trust_root_paths;
    }

    public String getTlsHostName() {
        return tls_host_name;
    }

    public void setTlsHostName(String tls_host_name) {
        this.tls_host_name = tls_host_name;
    }
}
