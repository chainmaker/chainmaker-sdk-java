/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk;

import io.grpc.ConnectivityState;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionPool {

    private static final int RETRY_INTERVAL = 500; //获取可用客户端连接对象重试时间间隔，单位：ms
    private static final int RETRY_LIMIT = 5; // 获取可用客户端连接对象最大重试次数

    private static final Logger logger = LoggerFactory.getLogger(ConnectionPool.class);

    private  List<RpcServiceClient> rpcServiceClients;
    // user's private key used to tls
    private PrivateKey privateKey;
    // user's certificate
    private Certificate certificate;

    public List<RpcServiceClient> getRpcServiceClients() {
        return rpcServiceClients;
    }

    public void setRpcServiceClients(List<RpcServiceClient> rpcServiceClients) {
        this.rpcServiceClients = rpcServiceClients;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public RpcServiceClient getConnection() {
        for (int i = 0; i <  RETRY_LIMIT; i++) {
            for(RpcServiceClient rpcServiceClient : rpcServiceClients) {
                ConnectivityState connectivityState = rpcServiceClient.getManagedChannel().getState(true);
                if (connectivityState.equals(ConnectivityState.IDLE) || connectivityState.equals(ConnectivityState.READY)){
                    return rpcServiceClient;
                }
            }
            try {
                Thread.sleep(RETRY_INTERVAL);
            } catch (InterruptedException e) {
                logger.error("Thread sleep error : ", e);
            }
        }
        //如果重试后还未获得，则提示重置连接数
        return null;
    }
}
