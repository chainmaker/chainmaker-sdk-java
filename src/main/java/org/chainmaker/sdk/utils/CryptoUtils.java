package org.chainmaker.sdk.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.TBSCertificate;
import org.bouncycastle.jcajce.provider.digest.Keccak;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.chainmaker.sdk.crypto.ChainMakerCryptoSuiteException;

import java.io.StringReader;
import java.security.PrivateKey;
import java.security.Security;

public class CryptoUtils {

    private CryptoUtils() {
        throw new IllegalStateException("Crypto Utils class");
    }
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public static PrivateKey getPrivateKeyFromBytes(byte[] pemKey) throws ChainMakerCryptoSuiteException {
        PrivateKey pk = null;

        try {
            PemReader pr = new PemReader(new StringReader(new String(pemKey)));
            PemObject po = pr.readPemObject();
            PEMParser pem = new PEMParser(new StringReader(new String(pemKey)));

            if (po.getType().equals("PRIVATE KEY")) {
                pk = new JcaPEMKeyConverter().getPrivateKey((PrivateKeyInfo) pem.readObject());
            } else {
                PEMKeyPair kp = (PEMKeyPair) pem.readObject();
                pk = new JcaPEMKeyConverter().getPrivateKey(kp.getPrivateKeyInfo());
            }
        } catch (Exception e) {
            throw new ChainMakerCryptoSuiteException(e.toString());
        }
        return pk;
    }

    public static String makeAddrFromCert(Certificate certificate) throws UtilsException {

        ByteArrayInputStream bIn = null;
        try {
            bIn = new ByteArrayInputStream(certificate.getEncoded());
        } catch (CertificateEncodingException e) {
            throw new UtilsException("certificate to ByteArrayInputStream err : " + e.getMessage());
        }
        ASN1InputStream aIn = new ASN1InputStream(bIn);

        ASN1Sequence seq = null;
        try {
            seq = (ASN1Sequence)aIn.readObject();
        } catch (IOException e) {
            throw new UtilsException("certificate to ASN1Sequence err : " + e.getMessage());
        }

        org.bouncycastle.asn1.x509.Certificate obj = org.bouncycastle.asn1.x509.Certificate.getInstance(seq);
        TBSCertificate tbsCertificate = obj.getTBSCertificate();
        Extensions ext = tbsCertificate.getExtensions();

        SubjectKeyIdentifier si = SubjectKeyIdentifier.fromExtensions(ext);
        String ski = Hex.toHexString(si.getKeyIdentifier());

        byte[] data = Hex.decode(ski);
        Keccak.DigestKeccak kecc = new Keccak.Digest256();
        kecc.update(data, 0, data.length);
        byte[] address = kecc.digest();
        String addr = Hex.toHexString(address);
        return "0x" + addr.substring(24);
    }

}
