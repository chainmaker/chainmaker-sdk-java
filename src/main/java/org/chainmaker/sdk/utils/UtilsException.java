package org.chainmaker.sdk.utils;

import org.chainmaker.sdk.SdkException;

public class UtilsException extends SdkException {
    public UtilsException(String message) {
        super(message);
    }
}
