/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.stub.StreamObserver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SHA3Digest;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.chainmaker.pb.accesscontrol.PolicyOuterClass;
import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.common.ContractOuterClass;
import org.chainmaker.pb.common.Request;
import org.chainmaker.pb.common.ResultOuterClass;
import org.chainmaker.pb.config.ChainConfigOuterClass;
import org.chainmaker.pb.discovery.Discovery;
import org.chainmaker.pb.store.Store;
import org.chainmaker.sdk.config.ArchiveConfig;
import org.chainmaker.sdk.crypto.ChainMakerCryptoSuiteException;
import org.chainmaker.sdk.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.cert.CertificateEncodingException;
import java.util.concurrent.TimeUnit;

import static org.chainmaker.pb.common.ContractOuterClass.CertManageFunction.*;

/*
 ChainClient is a client used to send transactions to chain nodes through rpc.
 */
public class ChainClient {
    // chainId is the identity of the chain
    private String chainId;
    // the nodes of the chain
    private List<Node> nodes;

    private ConnectionPool connectionPool;

    // the user used to sign transactions
    private User clientUser;
    private boolean isEnabledCertHash;

    private ArchiveConfig archiveConfig;

    public User getClientUser() {
        return clientUser;
    }

    public void setClientUser(User clientUser) {
        this.clientUser = clientUser;
    }

    public boolean isEnabledCertHash() {
        return isEnabledCertHash;
    }

    public void setEnabledCertHash(boolean enabledCertHash) {
        isEnabledCertHash = enabledCertHash;
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public ConnectionPool getConnectionPool() {
        return connectionPool;
    }

    public void setConnectionPool(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    public ArchiveConfig getArchiveConfig() {
        return archiveConfig;
    }

    public void setArchiveConfig(ArchiveConfig archiveConfig) {
        this.archiveConfig = archiveConfig;
    }

    private static final String TX_ID = "txId";
    private static final String ORG_ID = "org_id";
    private static final String NODE_IDS = "node_ids";
    private static final String BLOCK_HEIGHT = "blockHeight";
    private static final String BLOCK_HASH = "blockHash";
    private static final String WITH_RW_SET = "withRWSet";
    private static final long DEFAULT_RPC_TIMEOUT = 10000;
    private static final long DEFAULT_SYNC_RESULT_TIMEOUT = 10000;

    private static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    private static final String MYSQL_DBNAME_PREFIX = "cm_archived_chain";
    private static final String MYSQL_TABLENAME_PREFIX = "t_block_info";
    private static final long ROWS_PREBLOCKINFO_TABLE = 100000;

    private static final Logger logger = LoggerFactory.getLogger(ChainClient.class);

    // add node to ChainClient
    public void addNode(Node node) {
        nodes.add(node);
    }

    // enable cert hash instead full cert to identify sender when sending transaction to chain node
    public synchronized boolean enableCertHash() throws ChainMakerCryptoSuiteException, ChainClientException {
        if (isEnabledCertHash) {
            return true;
        }
        // get certHash if needed
        if (clientUser.getCertHash() == null || clientUser.getCertHash().length == 0) {
            ChainConfigOuterClass.ChainConfig chainConfig = getChainConfig(DEFAULT_RPC_TIMEOUT);
            if (chainConfig == null) {
                throw new ChainClientException("get chain config from node failed, please try again later");
            }
            clientUser.setCertHash(getCertificateId(chainConfig.getCrypto().getHash()));
            if (clientUser.getCertHash() == null || clientUser.getCertHash().length == 0) {
                throw new ChainClientException("get certificate id failed");
            }
        }
        // check cert hash
        if (checkCertHashOnChain()) {
            isEnabledCertHash = true;
            return true;
        }
        ResponseInfo responseInfo = addCert(DEFAULT_RPC_TIMEOUT, DEFAULT_SYNC_RESULT_TIMEOUT);
        if (responseInfo == null || responseInfo.getTxResponse().getCode() != ResultOuterClass.TxStatusCode.SUCCESS
            || responseInfo.getTxResponse().getContractResult().getCode() != ResultOuterClass.ContractResultCode.OK) {
            throw new ChainClientException("add cert failed");
        }
        // check cert hash
        for (int i = 0; i < 5; i++) {
            if (checkCertHashOnChain()) {
                isEnabledCertHash = true;
                return true;
            }
        }
        return false;
    }

    // ## User Contract
    // ### 1.1 Create payload of contract creation request
    // **参数说明**
    //   - contractName: 合约名
    //   - version: 版本号
    //   - runtimeType: 合约运行环境
    //   - params: 合约初始化参数
    //   - byteCodes: 合约字节数组
    // **返回值说明**
    // 返回payload字节数组
    public byte[] createPayloadOfContractCreation(String contractName, String version,
                                                  ContractOuterClass.RuntimeType runtimeType, Map<String, String> params,
                                                  byte[] byteCodes) {
        return createPayloadOfContractMgmt(contractName, version, runtimeType,
                ContractOuterClass.ManageUserContractFunction.INIT_CONTRACT.name(), params, byteCodes);
    }

    // ### 1.2 Create payload of contract upgrade request
    // **参数说明**
    //   - contractName: 合约名
    //   - version: 版本号
    //   - runtimeType: 合约运行环境
    //   - params: 合约初始化参数
    //   - byteCodes: 合约字节数组
    public byte[] createPayloadOfContractUpgrade(String contractName, String version,
                                                 ContractOuterClass.RuntimeType runtimeType, Map<String, String> params,
                                                 byte[] byteCodes) {
        return createPayloadOfContractMgmt(contractName, version, runtimeType,
                ContractOuterClass.ManageUserContractFunction.UPGRADE_CONTRACT.name(), params, byteCodes);
    }

    // ### 1.3 Create payload of contract freeze request
    // **参数说明**
    //   - contractName: 合约名
    public byte[] createPayloadOfContractFreeze(String contractName) {
        // create request payload
        return createPayloadOfContractMgmt(contractName, null, null,
                ContractOuterClass.ManageUserContractFunction.FREEZE_CONTRACT.name(), null, null);
    }

    // ### 1.4 生成解冻合约待签名的payload
    // **参数说明**
    //   - contractName: 合约名
    public byte[] createPayloadOfContractUnfreeze(String contractName) {
        // create request payload
        return createPayloadOfContractMgmt(contractName, null, null,
                ContractOuterClass.ManageUserContractFunction.UNFREEZE_CONTRACT.name(), null, null);
    }

    // ### 1.5 生成吊销合约待签名的payload
    // **参数说明**
    //   - contractName: 合约名
    public byte[] createPayloadOfContractRevoke(String contractName) {
        // create request payload
        return createPayloadOfContractMgmt(contractName,null, null,
                ContractOuterClass.ManageUserContractFunction.REVOKE_CONTRACT.name(),null, null);
    }

    // ### 1.6 将多个带签名后的payload合并成一个带多个签名的payload，所有签名都放在一个payload后面
    // **参数说明**
    //   - payloads: 多个带签名的payload
    // **返回值说明**
    // 返回带多个签名的payload字节数组
    public byte[] mergeSignedPayloadsOfContractMgmt(byte[][] payloads) throws ChainClientException {
        if (payloads.length == 0) {
            throw new ChainClientException("payloads empty");
        }
        Request.ContractMgmtPayload allPayload;
        Request.ContractMgmtPayload.Builder allPayloadBuilder;

        try {
            allPayload = Request.ContractMgmtPayload.parseFrom(payloads[0]);
        } catch (InvalidProtocolBufferException e) {
            logger.error("ContractMgmtPayload parseFrom result : ", e);
            throw new ChainClientException("ContractMgmtPayload parseFrom result : " + e.getMessage());
        }
        allPayloadBuilder = allPayload.toBuilder();
        if (allPayloadBuilder.getEndorsementList().size() != 1) {
            throw new ChainClientException("the count of payload endorsement is not 1");
        }
        for (int i = 1; i < payloads.length; i++) {
            Request.ContractMgmtPayload payload = null;
            try {
                payload = Request.ContractMgmtPayload.parseFrom(payloads[i]);
            } catch (InvalidProtocolBufferException e) {
                logger.error("ContractMgmtPayload parseFrom result : ", e);
                throw new ChainClientException("ContractMgmtPayload parseFrom result : " + e.getMessage());
            }
            if (payload.getEndorsementList().size() != 1) {
                throw new ChainClientException("the count of payload endorsement is not 1");
            }
            allPayloadBuilder.addEndorsement(payload.getEndorsementList().get(0));
        }
        return allPayloadBuilder.build().toByteArray();
    }

    // ### 1.7 创建合约
    // **参数说明**
    //   - payloadWithEndorsementsBytes: 带签名的合约内容
    //   - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
    //   - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，直接返回（返回信息里包含交易ID），单位：毫秒
    public ResponseInfo createContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                                      long syncResultTimeout) throws
            ChainMakerCryptoSuiteException, ChainClientException {
        ResponseInfo responseInfo = sendRequest(Request.TxType.MANAGE_USER_CONTRACT, payloadWithEndorsementsBytes, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }

        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 1.8 升级合约
    // **参数说明**
    //   - payloadWithEndorsementsBytes: 带签名的合约内容
    //   - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
    //   - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，直接返回（返回信息里包含交易ID）,单位：毫秒
    public ResponseInfo upgradeContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                        long syncResultTimeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        ResponseInfo responseInfo = sendRequest(Request.TxType.MANAGE_USER_CONTRACT,
                payloadWithEndorsementsBytes, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }

        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 1.9 冻结合约
    // **参数说明**
    //   - payloadWithEndorsementsBytes: 带签名的合约内容
    //   - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
    //   - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，直接返回（返回信息里包含交易ID）,单位：毫秒
    public ResponseInfo freezeContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                        long syncResultTimeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        ResponseInfo responseInfo = sendRequest(Request.TxType.MANAGE_USER_CONTRACT,
                payloadWithEndorsementsBytes, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }

        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 1.10 解冻合约
    // **参数说明**
    //   - payloadWithEndorsementsBytes: 带签名的合约内容
    //   - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
    //   - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，直接返回（返回信息里包含交易ID）,单位：毫秒
    public ResponseInfo unfreezeContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                       long syncResultTimeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        ResponseInfo responseInfo = sendRequest(Request.TxType.MANAGE_USER_CONTRACT,
                payloadWithEndorsementsBytes, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }

        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 1.11 吊销合约
    // **参数说明**
    //   - payloadWithEndorsementsBytes: 带签名的合约内容
    //   - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
    //   - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，直接返回（返回信息里包含交易ID）,单位：毫秒
    public ResponseInfo revokeContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                       long syncResultTimeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        ResponseInfo responseInfo = sendRequest(Request.TxType.MANAGE_USER_CONTRACT,
                payloadWithEndorsementsBytes, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }

        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 1.12 执行合约接口
    // **参数说明**
    //   - contractName: 合约名
    //   - method: 方法名
    //   - params: 执行参数
    //   - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
    //   - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，直接返回（返回信息里包含交易ID），单位：毫秒
    public ResponseInfo invokeContract(String contractName, String method,
                                                      Map<String, String> params, long rpcCallTimeout,
                                                      long syncResultTimeout) throws
            ChainMakerCryptoSuiteException, ChainClientException {
        byte[] payloadOfTransaction = createPayloadOfTransaction(contractName, method, params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.INVOKE_USER_CONTRACT,
                payloadOfTransaction, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }
        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    public ResponseInfo invokeContract(String contractName, String method, String txid,
                                       Map<String, String> params, long rpcCallTimeout,
                                       long syncResultTimeout) throws InvalidProtocolBufferException,
            ChainMakerCryptoSuiteException, ChainClientException {
        byte[] payloadOfTransaction = createPayloadOfTransaction(contractName, method, params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.INVOKE_USER_CONTRACT,
                payloadOfTransaction, txid,rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }
        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 1.13 查询合约接口
    // **参数说明**
    //   - contractName: 合约名
    //   - method: 方法名
    //   - params: 执行参数
    //   - timeout: 执行超时时间
    public ResponseInfo queryContract(String contractName, String method,
                                                      Map<String, String> params, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {

        byte[] payloadOfQuery = createPayloadOfQuery(contractName, method, params);

        return sendRequest(Request.TxType.QUERY_USER_CONTRACT, payloadOfQuery, timeout);
    }

    // ## 2 系统合约接口
    // ### 2.1 根据交易Id查询交易
    // **参数说明**
    //   - txId: 交易ID
    public ChainmakerTransaction.TransactionInfo getTxByTxId(String txId, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        Map<String, String> params = new HashMap<>();
        params.put(TX_ID, txId);

        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_TX_BY_TX_ID.toString(), params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return ChainmakerTransaction.TransactionInfo.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("transactionInfo parseFrom result : ", e);
            throw new ChainClientException("transactionInfo parseFrom result : " + e.getMessage());
        }
    }

    // ### 2.2 根据区块高度查询区块
    // **参数说明**
    //   - blockHeight: 区块高度
    //   - withRWSet: 是否返回读写集
    public ChainmakerBlock.BlockInfo getBlockByHeight(long blockHeight, boolean withRWSet, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {

        Map<String, String> params = new HashMap<>();
        params.put(BLOCK_HEIGHT, Long.toString(blockHeight));
        params.put(WITH_RW_SET, Boolean.toString(withRWSet));

        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_BLOCK_BY_HEIGHT.toString(), params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return ChainmakerBlock.BlockInfo.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("blockInfo parseFrom result : ", e);
            throw new ChainClientException("blockInfo parseFrom result : " + e.getMessage());
        }
    }

    // ### 2.3 根据区块哈希查询区块
    // **参数说明**
    //   - blockHash: 区块hash
    //   - withRWSet: 是否返回读写集
    public ChainmakerBlock.BlockInfo getBlockByHash(String blockHash, boolean withRWSet, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        Map<String, String> params = new HashMap<>();
        params.put(BLOCK_HASH, blockHash);
        params.put(WITH_RW_SET, Boolean.toString(withRWSet));

        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_BLOCK_BY_HASH.toString(), params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return ChainmakerBlock.BlockInfo.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("blockInfo parseFrom result : ", e);
            throw new ChainClientException("blockInfo parseFrom result : " + e.getMessage());
        }
    }

    // ### 2.4 根据交易Id查询区块
    // **参数说明**
    //   - txId: 交易Id
    //   - withRWSet: 是否返回读写集
    public ChainmakerBlock.BlockInfo getBlockByTxId(String txId, boolean withRWSet, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        Map<String, String> params = new HashMap<>();
        params.put(TX_ID, txId);
        params.put(WITH_RW_SET, Boolean.toString(withRWSet));

        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_BLOCK_BY_TX_ID.toString(), params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return ChainmakerBlock.BlockInfo.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("blockInfo parseFrom result : ", e);
            throw new ChainClientException("blockInfo parseFrom result : " + e.getMessage());
        }
    }

    // ### 2.5 查询上一个配置块
    // **参数说明**
    //   - withRWSet: 是否返回读写集
    public ChainmakerBlock.BlockInfo getLastConfigBlock(boolean withRWSet, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        Map<String, String> params = new HashMap<>();
        params.put(WITH_RW_SET, Boolean.toString(withRWSet));

        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_LAST_CONFIG_BLOCK.toString(), params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return ChainmakerBlock.BlockInfo.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("blockInfo parseFrom result : ", e);
            throw new ChainClientException("blockInfo parseFrom result : " + e.getMessage());
        }
    }

    // ### 2.6 查询节点加入的链信息
    // **参数说明**
    //    - 返回ChainId清单
    public Discovery.ChainList getNodeChainList(long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_NODE_CHAIN_LIST.toString(), null);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return Discovery.ChainList.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("chainList parseFrom result : ", e);
            throw new ChainClientException("chainList parseFrom result : " + e.getMessage());
        }
    }

    // ### 2.7 查询链信息
    // **参数说明**
    //   - 包括：当前链最新高度，链节点信息
    public Discovery.ChainInfo getChainInfo(long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_CHAIN_INFO.toString(), null);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return Discovery.ChainInfo.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("chainInfo parseFrom result : ", e);
            throw new ChainClientException("chainInfo parseFrom result : " + e.getMessage());
        }
    }

    // ### 2.8 根据txId查询区块高度
    // **参数说明**
    //   - txId: 交易id
    public long getBlockHeightByTxId(String txId, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        return getBlockHeight(txId, null, timeout);
    }

    // ### 2.9 根据blockHash查询区块高度
    // **参数说明**
    //   - 区块哈希: blockHash
    public long getBlockHeightByBlockHash(String blockHash, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        return getBlockHeight(null, blockHash, timeout);
    }

    // ### 2.10 根据区块高度查询完整区块
    // **参数说明**
    //   - 区块高度: blockHeight
    public Store.BlockWithRWSet getFullBlockByHeight(long blockHeight, long timeout) throws ChainClientException, ChainMakerCryptoSuiteException {
        Map<String, String> params = new HashMap<>();
        params.put(BLOCK_HEIGHT, Long.toString(blockHeight));
        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_FULL_BLOCK_BY_HEIGHT.toString(), params);
        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }

        try {
            return Store.BlockWithRWSet.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("blockWithRWSet parseFrom result : ", e);
            throw new ChainClientException("blockWithRWSet parseFrom result : " + e.getMessage());
        }
    }

    // ### 2.11 查询最新区块信息
    // **参数说明**
    //   - withRWSet: 是否返回读写集
    public ChainmakerBlock.BlockInfo getLastBlcok(boolean withRWSet, long timeout) throws ChainClientException, ChainMakerCryptoSuiteException {
        Map<String, String> params = new HashMap<>();
        params.put(WITH_RW_SET, Boolean.toString(withRWSet));

        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_LAST_BLOCK.toString(), params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return ChainmakerBlock.BlockInfo.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("BlockInfo parseFrom result : ", e);
            throw new ChainClientException("BlockInfo parseFrom result : " + e.getMessage());
        }
    }

    // ### 2.12 查询最新区块高度
    public long getCurrentBlockHeight(long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        ChainmakerBlock.BlockInfo blockInfo = getLastBlcok(false, timeout);
        return blockInfo.getBlock().getHeader().getBlockHeight();
    }

    // ### 2.13 根据区块高度查询区块头
    public ChainmakerBlock.BlockHeader getBlockHeaderByHeight(long blockHeight, long timeout) throws ChainClientException, ChainMakerCryptoSuiteException {
        Map<String, String> params = new HashMap<>();
        params.put(BLOCK_HEIGHT, Long.toString(blockHeight));

        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                ContractOuterClass.QueryFunction.GET_BLOCK_HEADER_BY_HEIGHT.toString(), params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }

        try {
            return ChainmakerBlock.BlockHeader.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("blockHeader parseFrom result : ", e);
            throw new ChainClientException("blockHeader parseFrom result : " + e.getMessage());
        }
    }

    // ## 3 链配置接口
    // ### 3.1 查询最新链配置
    public ChainConfigOuterClass.ChainConfig getChainConfig(long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.GET_CHAIN_CONFIG.toString(), null);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null ||
                responseInfo.getTxResponse().getCode() != ResultOuterClass.TxStatusCode.SUCCESS) {
            return null;
        }
        try {
            return ChainConfigOuterClass.ChainConfig.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("chainConfig parseFrom result : ", e);
            throw new ChainClientException("chainConfig parseFrom result : " + e.getMessage());
        }
    }

    // ### 3.2 根据指定区块高度查询最近链配置
    //   - 如果当前区块就是配置块，直接返回当前区块的链配置
    public ChainConfigOuterClass.ChainConfig getChainConfigByBlockHeight(int blockHeight, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        Map<String, String> params = new HashMap<>();
        params.put("block_height", String.valueOf(blockHeight));
        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.GET_CHAIN_CONFIG_AT.toString(), params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return ChainConfigOuterClass.ChainConfig.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("ChainConfig parseFrom result : ", e);
            throw new ChainClientException("ChainConfig parseFrom result : " + e.getMessage());
        }
    }

    // ### 3.3 查询最新链配置序号Sequence
    //   - 用于链配置更新
    public long getChainConfigSequence(long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.GET_CHAIN_CONFIG.toString(), null);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        ChainConfigOuterClass.ChainConfig chainConfig = null;
        try {
            chainConfig = ChainConfigOuterClass.ChainConfig.parseFrom(
                    responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("chainConfig parseFrom result : ", e);
            throw new ChainClientException("chainConfig parseFrom result : " + e.getMessage());
        }

        return chainConfig.getSequence();
    }

    // ### 3.4 更新Core模块待签名payload生成
    // **参数说明**
    //   - txSchedulerTimeout: 交易调度器从交易池拿到交易后, 进行调度的时间，其值范围为[0, 60]，若无需修改，请置为-1
    //   - txSchedulerValidateTimeout: 交易调度器从区块中拿到交易后, 进行验证的超时时间，其值范围为[0, 60]，若无需修改，请置为-1
    public byte[] createPayloadOfChainConfigCoreUpdate(int txSchedulerTimeout, int txSchedulerValidateTimeout, long timeout)
            throws ChainClientException, ChainMakerCryptoSuiteException {
        if (txSchedulerTimeout > 60 || txSchedulerValidateTimeout > 60) {
            throw new ChainClientException("invalid txSchedulerTimeout or txSchedulerValidateTimeout");
        }
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        if (txSchedulerTimeout > 0) {
            params.put("tx_scheduler_timeout", String.valueOf(txSchedulerTimeout));
        }
        if (txSchedulerValidateTimeout > 0) {
            params.put("tx_scheduler_validate_timeout", String.valueOf(txSchedulerTimeout));
        }

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.CORE_UPDATE.toString(), sequence + 1, params);
    }

    // ### 3.5 更新Core模块待签名payload生成
    // **参数说明**
    //   - txTimestampVerify: 是否需要开启交易时间戳校验
    //   - (以下参数，若无需修改，请置为-1)
    //   - txTimeout: 交易时间戳的过期时间(秒)，其值范围为[600, +∞)
    //   - blockTxCapacity: 区块中最大交易数，其值范围为(0, +∞]
    //   - blockSize: 区块最大限制，单位MB，其值范围为(0, +∞]
    //   - blockInterval: 出块间隔，单位:ms，其值范围为[10, +∞]
    public byte[] createPayloadOfChainConfigBlockUpdate(boolean txTimestampVerify, int txTimeout, int blockTxCapacity,
                                                        int blockSize, int blockInterval, long timeout)
            throws ChainClientException, ChainMakerCryptoSuiteException {
        if (txTimeout < 600 || blockTxCapacity < 1 || blockSize < 1 || blockInterval < 10) {
            throw new ChainClientException("invalid parameters");
        }
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put("tx_timestamp_verify", String.valueOf(txTimestampVerify));

        params.put("tx_timeout", String.valueOf(txTimeout));
        params.put("block_tx_capacity", String.valueOf(blockTxCapacity));
        params.put("block_size", String.valueOf(blockSize));
        params.put("block_interval", String.valueOf(blockInterval));

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.BLOCK_UPDATE.toString(), sequence + 1, params);
    }

    // ### 3.6 添加信任组织根证书待签名payload生成
    // **参数说明**
    //   - trustRootOrgId: 组织Id
    //   - trustRootCrt: 根证书
    public byte[] createPayloadOfChainConfigTrustRootAdd(String trustRootOrgId, String trustRootCrt, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(ORG_ID, trustRootOrgId);
        params.put("root", trustRootCrt);

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.TRUST_ROOT_ADD.toString(), sequence + 1, params);
    }

    // ### 3.7 更新信任组织根证书待签名payload生成
    // **参数说明**
    //   - trustRootOrgId: 组织Id
    //   - trustRootCrt: 根证书
    public byte[] createPayloadOfChainConfigTrustRootUpdate(String trustRootOrgId, String trustRootCrt, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(ORG_ID, trustRootOrgId);
        params.put("root", trustRootCrt);

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.TRUST_ROOT_UPDATE.toString(), sequence + 1, params);
    }

    // ### 3.8 删除信任组织根证书待签名payload生成
    // **参数说明**
    //   - trustRootOrgId: 组织Id
    public byte[] createPayloadOfChainConfigTrustRootDelete(String trustRootOrgId, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(ORG_ID, trustRootOrgId);

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.TRUST_ROOT_DELETE.toString(), sequence + 1, params);
    }

    // ### 3.9 添加权限配置待签名payload生成
    // **参数说明**
    //   - permissionResourceName: 权限名
    //   - principle: 权限规则
    public byte[] createPayloadOfChainConfigPermissionAdd(String permissionResourceName,
                                                          PolicyOuterClass.Policy principal, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(permissionResourceName, principal.toString());

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.PERMISSION_ADD.toString(), sequence + 1, params);

    }

    // ### 3.10 更新权限配置待签名payload生成
    // **参数说明**
    //   - permissionResourceName: 权限名
    //   - principle: 权限规则
    public byte[] createPayloadOfChainConfigPermissionUpdate(String permissionResourceName,
                                                             PolicyOuterClass.Policy principal, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {

        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(permissionResourceName, principal.toString());

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.PERMISSION_UPDATE.toString(), sequence + 1, params);

    }

    // ### 3.11 删除权限配置待签名payload生成
    // **参数说明**
    //   - permissionResourceName: 权限名
    public byte[] createPayloadOfChainConfigPermissionDelete(String permissionResourceName, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(permissionResourceName, "");

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.PERMISSION_DELETE.toString(), sequence + 1, params);
    }

    // ### 3.12 添加共识节点地址待签名payload生成
    // **参数说明**
    //   - nodeOrgId: 节点组织Id
    //   - nodeAddresses: 节点地址
    public byte[] createPayloadOfChainConfigConsensusNodeAddrAdd(String nodeOrgId, String[] nodeAddresses, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(ORG_ID, nodeOrgId);
        if (nodeAddresses.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(nodeAddresses[0]);
            for (int i = 1; i < nodeAddresses.length; i++) {
                stringBuilder.append(',').append(nodeAddresses[i]);
            }
            params.put(NODE_IDS, stringBuilder.toString());
        }

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.NODE_ID_ADD.toString(), sequence + 1, params);
    }

    // ### 3.13 更新共识节点地址待签名payload生成
    // **参数说明**
    //   - nodeOrgId: 节点组织Id
    //   - nodeOldAddress: 节点原地址
    //   - nodeNewAddress: 节点新地址
    public byte[] createPayloadOfChainConfigConsensusNodeAddrUpdate(String nodeOrgId, String nodeOldAddress,
                                                                    String nodeNewAddress, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(ORG_ID, nodeOrgId);
        params.put("node_id", nodeOldAddress);
        params.put("new_node_id", nodeNewAddress);
        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.NODE_ID_UPDATE.toString(), sequence + 1, params);

    }

    // ### 3.14 删除共识节点地址待签名payload生成
    // **参数说明**
    //   - nodeOrgId: 节点组织Id
    //   - nodeAddress: 节点地址
    public byte[] createPayloadOfChainConfigConsensusNodeAddrDelete(String nodeOrgId, String nodeAddress, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(ORG_ID, nodeOrgId);
        params.put("node_id", nodeAddress);
        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.NODE_ID_DELETE.toString(), sequence + 1, params);
    }

    // ### 3.15 添加共识节点待签名payload生成
    // **参数说明**
    //   - nodeOrgId: 节点组织Id
    //   - nodeAddresses: 节点地址
    public byte[] createPayloadOfChainConfigConsensusNodeOrgAdd(String nodeOrgId, String[] nodeAddresses, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(ORG_ID, nodeOrgId);
        if (nodeAddresses.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(nodeAddresses[0]);
            for (int i = 1; i < nodeAddresses.length; i++) {
                stringBuilder.append(',').append(nodeAddresses[i]);
            }
            params.put(NODE_IDS, stringBuilder.toString());
        }

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.NODE_ORG_ADD.toString(), sequence + 1, params);

    }

    // ### 3.16 更新共识节点待签名payload生成
    // **参数说明**
    //   - nodeOrgId: 节点组织Id
    //   - nodeAddresses: 节点地址
    public byte[] createPayloadOfChainConfigConsensusNodeOrgUpdate(String nodeOrgId, String[] nodeAddresses, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(ORG_ID, nodeOrgId);
        if (nodeAddresses.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(nodeAddresses[0]);
            for (int i = 1; i < nodeAddresses.length; i++) {
                stringBuilder.append(',').append(nodeAddresses[i]);
            }
            params.put(NODE_IDS, stringBuilder.toString());
        }

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.NODE_ORG_UPDATE.toString(), sequence + 1, params);

    }

    // ## 3 链配置接口
    // ### 3.17 删除共识节点待签名payload生成
    // **参数说明**
    //   - nodeOrgId: 节点组织Id
    public byte[] createPayloadOfChainConfigConsensusNodeOrgDelete(String nodeOrgId, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {

        long sequence = getChainConfigSequence(timeout);
        Map<String, String> params = new HashMap<>();
        params.put(ORG_ID, nodeOrgId);
        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.NODE_ORG_DELETE.toString(), sequence + 1, params);

    }

    // ## 3 链配置接口
    // ### 3.18 添加共识扩展字段待签名payload生成
    // **参数说明**
    //   - params: Map<String, String>
    public byte[] createPayloadOfChainConfigConsensusExtAdd(Map<String, String> params, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {

        long sequence = getChainConfigSequence(timeout);
        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.CONSENSUS_EXT_ADD.toString(), sequence + 1, params);
    }

    // ## 3 链配置接口
    // ### 3.19 添加共识扩展字段待签名payload生成
    // **参数说明**
    //   - params: Map<String, String>
    public byte[] createPayloadOfChainConfigConsensusExtUpdate(Map<String, String> params, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        long sequence = getChainConfigSequence(timeout);
        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.CONSENSUS_EXT_UPDATE.toString(), sequence + 1, params);
    }

    // ## 3 链配置接口
    // ### 3.20 添加共识扩展字段待签名payload生成
    // **参数说明**
    //   - keys: 待删除字段
    public byte[] createPayloadOfChainConfigConsensusExtDelete(String[] keys, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {

        Map<String, String> params = new HashMap<>();
        if (keys.length > 0) {
            for(int i = 0; i < keys.length; i++) {
                params.put(keys[i], "");
            }
        }
        long sequence = getChainConfigSequence(timeout);
        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CHAIN_CONFIG.toString(),
                ContractOuterClass.ConfigFunction.CONSENSUS_EXT_DELETE.toString(), sequence + 1, params);

    }

    // ## 3 链配置接口
    // ### 3.21 系统合约Payload签名收集&合并
    public byte[] mergeSignedPayloadsOfSystemContract(byte[][] payloads) throws ChainClientException {
        if (payloads.length == 0) {
            throw new ChainClientException("payloads is empty");
        }
        Request.SystemContractPayload systemContractPayload;
        Request.SystemContractPayload.Builder configUpdatePayloadBuilder;

        try {
            systemContractPayload = Request.SystemContractPayload.parseFrom(payloads[0]);
        } catch (InvalidProtocolBufferException e) {
            logger.error("systemContractPayload parseFrom result : ", e);
            throw new ChainClientException("systemContractPayload parseFrom result : " + e.getMessage());
        }
        configUpdatePayloadBuilder = systemContractPayload.toBuilder();
        if (configUpdatePayloadBuilder.getEndorsementList().size() != 1) {
            throw new ChainClientException("count of payload's endorsement is not 1");
        }
        for (int i = 1; i < payloads.length; i++) {
            Request.SystemContractPayload payload = null;
            try {
                payload = Request.SystemContractPayload.parseFrom(payloads[i]);
            } catch (InvalidProtocolBufferException e) {
                logger.error("systemContractPayload parseFrom result : ", e);
                throw new ChainClientException("systemContractPayload parseFrom result : " + e.getMessage());
            }
            if (payload.getEndorsementList().size() != 1) {
                throw new ChainClientException("count of payload's endorsement is not 1");
            }
            configUpdatePayloadBuilder.addEndorsement(payload.getEndorsementList().get(0));
        }
        return configUpdatePayloadBuilder.build().toByteArray();
    }

    // ## 3 链配置接口
    // ### 3.22 发送链配置更新请求
    public ResponseInfo updateChainConfig(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        return sendRequest(Request.TxType.UPDATE_CHAIN_CONFIG, payloadWithEndorsementsBytes, rpcCallTimeout);
    }


    // ## 4 证书管理接口
    // ### 4.1 用户证书添加
    // **参数说明**
    //   - 在pb.TxResponse.ContractResult.Result字段中返回成功添加的certHash
    public ResponseInfo addCert(long rpcCallTimeout, long syncResultTimeout) throws
            ChainMakerCryptoSuiteException, ChainClientException {
        byte[] payloadOfTransaction = createPayloadOfSystemContract(
                ContractOuterClass.ContractName.SYSTEM_CONTRACT_CERT_MANAGE.toString(), CERT_ADD.toString(), 0, null);
        ResponseInfo responseInfo = sendRequest(Request.TxType.INVOKE_SYSTEM_CONTRACT, payloadOfTransaction, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }
        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 4.2 用户证书删除
    // **参数说明**
    //   - certHashes: 证书Hash列表
    public ResponseInfo deleteCert(String[] certHashes, long rpcCallTimeout, long syncResultTimeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        Map<String, String> params = new HashMap<>();
        if (certHashes.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(certHashes[0]);
            for (int i = 1; i < certHashes.length; i++) {
                stringBuilder.append(",").append(certHashes[i]);
            }
            params.put("cert_hashes", stringBuilder.toString());
        }

        byte[] payloadOfTransaction = createPayloadOfSystemContract(
                ContractOuterClass.ContractName.SYSTEM_CONTRACT_CERT_MANAGE.toString(), CERTS_DELETE.toString(), 0, params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.INVOKE_SYSTEM_CONTRACT, payloadOfTransaction, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }
        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 4.3 创建用户证书冻结payload
    // **参数说明**
    //   - certs: 冻结的证书内容列表
    public byte[] createPayloadOfFreezeCerts(String[] certs) {
        Map<String, String> params = new HashMap<>();
        if (certs.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(certs[0]);
            for (int i = 1; i < certs.length; i++) {
                stringBuilder.append(",").append(certs[i]);
            }
            params.put("certs", stringBuilder.toString());
        }

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CERT_MANAGE.toString(),
                CERTS_FREEZE.name(), 0, params);
    }

    // ### 4.4 创建用户证书解冻
    // **参数说明**
    //   - certs: 解冻的证书内容列表
    public byte[] createPayloadOfUnfreezeCerts(String[] certs) {
        Map<String, String> params = new HashMap<>();
        if (certs.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(certs[0]);
            for (int i = 1; i < certs.length; i++) {
                stringBuilder.append(",").append(certs[i]);
            }
            params.put("certs", stringBuilder.toString());
        }

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CERT_MANAGE.toString(),
                CERTS_UNFREEZE.name(), 0, params);
    }

    // ### 4.5 创建用户证书吊销payload
    // **参数说明**
    //   - certCrl: 吊销的证书列表
    public byte[] createPayloadOfRevokeCerts(String certCrl) {
        Map<String, String> params = new HashMap<>();
        params.put("cert_crl", certCrl);

        return createPayloadOfSystemContract(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CERT_MANAGE.toString(),
                CERTS_REVOKE.name(), 0, params);
    }

    // ### 4.6 证书冻结
    // **参数说明**
    //   - payload: admin签名之后的payload
    public ResponseInfo freezeCerts(byte[] payload, long rpcCallTimeout, long syncResultTimeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        ResponseInfo responseInfo = sendRequest(Request.TxType.INVOKE_SYSTEM_CONTRACT, payload, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }
        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 4.7 证书解冻
    // **参数说明**
    //   - - payload: admin签名之后的payload
    public ResponseInfo unfreezeCerts(byte[] payload, long rpcCallTimeout, long syncResultTimeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        ResponseInfo responseInfo = sendRequest(Request.TxType.INVOKE_SYSTEM_CONTRACT, payload, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }
        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 4.8 证书吊销
    // **参数说明**
    //   - payload: admin签名之后的payload
    public ResponseInfo revokeCerts(byte[] payload, long rpcCallTimeout, long syncResultTimeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        ResponseInfo responseInfo = sendRequest(Request.TxType.INVOKE_SYSTEM_CONTRACT, payload, rpcCallTimeout);
        if (syncResultTimeout <= 0) {
            return responseInfo;
        }
        if (responseInfo.getTxId() == null) {
            return responseInfo;
        }

        responseInfo.setSyncResultTxInfo(loopQueryResultByTxId(responseInfo.getTxId(), syncResultTimeout));

        return responseInfo;
    }

    // ### 4.9 用户证书查询
    // **参数说明**
    //   - certHashes: 证书Hash列表
    // 返回值说明：
    //   - *pb.CertInfos: 包含证书Hash和证书内容的列表
    public ResultOuterClass.CertInfos queryCert(String[] certHashes, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {
        Map<String, String> params = new HashMap<>();
        if (certHashes.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(certHashes[0]);
            for (int i = 1; i < certHashes.length; i++) {
                stringBuilder.append(",").append(certHashes[i]);
            }
            params.put("cert_hashes", stringBuilder.toString());
        }
        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_CERT_MANAGE.toString(),
                CERTS_QUERY.toString(), params);
        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return null;
        }
        try {
            return ResultOuterClass.CertInfos.parseFrom(responseInfo.getTxResponse().getContractResult().getResult());
        } catch (InvalidProtocolBufferException e) {
            logger.error("certInfos parseFrom result : ", e);
            throw new ChainClientException("certInfos parseFrom result : " + e.getMessage());
        }
    }
    // ## 5 多签接口

    // ## 6 消息订阅接口
    // ### 6.1 区块订阅
    // **参数说明**
    //   - startBlock: 订阅起始区块高度，若为-1，表示订阅实时最新区块
    //   - endBlock: 订阅结束区块高度，若为-1，表示订阅实时最新区块
    //   - withRwSet: 是否返回读写集
    public void subscribeBlock(long startBlock, long endBlock, boolean withRwSet,
                                StreamObserver<ResultOuterClass.SubscribeResult> blockStreamObserver) throws ChainClientException {
        byte[] payloadOfSubscribe = createPayloadOfSubscribe(startBlock, endBlock, withRwSet);
        RpcServiceClient rpcServiceClient = connectionPool.getConnection();
        try {
            rpcServiceClient.getRpcNodeStub().subscribe(createTxRequest(
                    Request.TxType.SUBSCRIBE_BLOCK_INFO, payloadOfSubscribe), blockStreamObserver);
        } catch (ChainMakerCryptoSuiteException e) {
            logger.error("subscribeBlock to peer error : {}", e.getMessage());
            throw new ChainClientException("subscribeBlock to peer error : " + e.getMessage());
        }
    }

    // ### 6.2 交易订阅
    // **参数说明**
    //   - startBlock: 订阅起始区块高度，若为-1，表示订阅实时最新区块
    //   - endBlock: 订阅结束区块高度，若为-1，表示订阅实时最新区块
    //   - txIds: 订阅txId列表，若为空，表示订阅所有txId
    public void subscribeTx(long startBlock, long endBlock, Request.TxType txType, String[] txIds,
                            StreamObserver<ResultOuterClass.SubscribeResult> txStreamObserver) throws ChainClientException {
        byte[] payloadOfSubscribe = createPayloadOfSubscribeTx(startBlock, endBlock, txType, txIds);

        RpcServiceClient rpcServiceClient = connectionPool.getConnection();
        try {
            rpcServiceClient.getRpcNodeStub().subscribe(createTxRequest(
                    Request.TxType.SUBSCRIBE_TX_INFO, payloadOfSubscribe), txStreamObserver);
        } catch (ChainMakerCryptoSuiteException e) {
            logger.error("subscribeTx to peer error : {}", e.getMessage());
            throw new ChainClientException("subscribeTx to peer error : " + e.getMessage());
        }
    }

    // ### 6.3 事件订阅
    // **参数说明**
    //   - topic: 订阅话题
    //   - contractName: 订阅合约名
    //   - startBlock: 订阅起始区块高度，若为-1，表示订阅实时最新区块
    //   - endBlock: 订阅结束区块高度，若为-1，表示订阅实时最新区块
    public void subscribeContractEvent(int startBlock, int endBlock, String topic, String contractName,
                                       StreamObserver<ResultOuterClass.SubscribeResult> contractEventStreamObserver) throws ChainClientException {
        byte[] payloadOfSubscribe = createPayloadOfSubscribeHistoryContractEvent(startBlock, endBlock, topic, contractName);

        RpcServiceClient rpcServiceClient = connectionPool.getConnection();
        try {
            rpcServiceClient.getRpcNodeStub().subscribe(createTxRequest(
                    Request.TxType.SUBSCRIBE_CONTRACT_EVENT_INFO, payloadOfSubscribe), contractEventStreamObserver);
        } catch (ChainMakerCryptoSuiteException e) {
            logger.error("subscribeContractEvent to peer error : {}", e.getMessage());
            throw new ChainClientException("subscribeContractEvent to peer error : " + e.getMessage());
        }
    }

    // ## 7 归档接口
    // ### 7.1 数据归档payload生成
    // **参数说明**
    //   - targetBlockHeight: 归档区块高度
    public byte[] createArchiveBlockPayload(long targetBlockHeight) {
        Request.ArchiveBlockPayload.Builder archiveBlockPayloadBuilder = Request.ArchiveBlockPayload.newBuilder().setBlockHeight(targetBlockHeight);
        return archiveBlockPayloadBuilder.build().toByteArray();
    }

    // ### 7.2 归档恢复payload生成
    // **参数说明**
    //   - fullBlock: 归档恢复数据
    public byte[] createRestoreBlockPayload(byte[] fullBlock) {
        Request.RestoreBlockPayload.Builder restoreBlockPayload = Request.RestoreBlockPayload.newBuilder().setFullBlock(ByteString.copyFrom(fullBlock));
        return restoreBlockPayload.build().toByteArray();
    }

    // ### 7.3 发送数据归档请求
    //   - payloadBytes: 数据归档payload
    public ResponseInfo sendArchiveBlockRequest(byte[] payloadBytes, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        return sendRequest(Request.TxType.ARCHIVE_FULL_BLOCK, payloadBytes, timeout);
    }

    // ### 7.4 发送归档恢复请求
    //   - payloadBytes: 归档恢复payload
    public ResponseInfo sendRestoreBlockRequest(byte[] payloadBytes, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        return sendRequest(Request.TxType.RESTORE_FULL_BLOCK, payloadBytes, timeout);
    }

    // ### 7.5 获取归档数据
    //   - targetBlockHeight: 归档区块
    public Store.BlockWithRWSet getArchivedFullBlockByHeight(long blockHeight) throws ChainClientException {
        return getFromArchiveStore(blockHeight);
    }

    // ### 7.6 获取归档区块信息
    //   - targetBlockHeight: 归档区块
    //   - withRWSet: 是否获取读写集
    public ChainmakerBlock.BlockInfo getArchivedBlockByHeight(long blockHeight, boolean withRWSet) throws ChainClientException {
        Store.BlockWithRWSet fullBlock = getFromArchiveStore(blockHeight);

        ChainmakerBlock.BlockInfo blockInfo = ChainmakerBlock.BlockInfo.newBuilder().setBlock(fullBlock.getBlock()).build();

        if (withRWSet){
            blockInfo.toBuilder().addAllRwsetList(blockInfo.getRwsetListList());
        }
        return blockInfo;
    }

    public ChainmakerBlock.BlockInfo getArchivedBlockByTxId(String txId, boolean withRWSet, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        long blockHeight = getBlockHeightByTxId(txId, timeout);
        return getArchivedBlockByHeight(blockHeight, withRWSet);
    }

    public ChainmakerBlock.BlockInfo getArchivedBlockByHash(String blockHash, boolean withRWSet, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        long blockHeight = getBlockHeightByBlockHash(blockHash, timeout);
        return getArchivedBlockByHeight(blockHeight, withRWSet);
    }

    public ChainmakerTransaction.TransactionInfo getArchivedTxByTxId(String txId, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        long blockHeight = getBlockHeightByTxId(txId, timeout);
        ChainmakerBlock.BlockInfo blockInfo = getArchivedBlockByHeight(blockHeight, false);
        for (int i = 0; i < blockInfo.getBlock().getTxsList().size(); i++){
            ChainmakerTransaction.Transaction tx = blockInfo.getBlock().getTxs(i);
            if (tx.getHeader().getTxId().equals(txId)){
                return ChainmakerTransaction.TransactionInfo.newBuilder().setTransaction(tx)
                        .setBlockHeight(blockInfo.getBlock().getHeader().getBlockHeight())
                        .setBlockHash(blockInfo.getBlock().getHeader().getBlockHash())
                        .setTxIndex(i).build();
            }
        }
        return null;
    }

    public Store.BlockWithRWSet getFromArchiveStore(long blockHeight) throws ChainClientException {
        if (archiveConfig.getType().equals("mysql")){
            return getArchivedBlockFromMySQL(blockHeight);
        }
        return null;
    }


    public Store.BlockWithRWSet getArchivedBlockFromMySQL(long blockHeight)
            throws ChainClientException {

        String archiveDest = archiveConfig.getDest();
        String[] destList = archiveDest.split(":");

        String user = destList[0];
        String pwd = destList[1];
        String host = destList[2];
        String port = destList[3];

        try {
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException e) {
            logger.error(DRIVER_NAME + " class not found : ", e);
            throw new ChainClientException("class not found : " + e.getMessage());
        }

        String url = String.format("jdbc:mysql://%s:%s/%s_%s?serverTimezone=GMT", host, port, MYSQL_DBNAME_PREFIX, chainId);

        byte[] blockWithRWSetBytes = null;
        String hmac = "";
        try {
            Connection connection = DriverManager.getConnection(url, user, pwd);
            Statement statement = connection.createStatement();
            String sql = String.format("SELECT Fblock_with_rwset, Fhmac from %s_%d WHERE Fblock_height = %d", MYSQL_TABLENAME_PREFIX, blockHeight/ROWS_PREBLOCKINFO_TABLE + 1, blockHeight);
            ResultSet resultSet = statement.executeQuery(sql);


            while (resultSet.next()){
                ResultSetMetaData metaData = resultSet.getMetaData();
                List<Object> dataList = new ArrayList<>();
                for (int i=1; i<metaData.getColumnCount()+1;i++){
                    if (metaData.getColumnName(i).equals("Fblock_with_rwset")){
                        blockWithRWSetBytes = (byte[]) resultSet.getObject(i);
                    }
                    if (metaData.getColumnName(i).equals("Fhmac")){
                        hmac = (String) resultSet.getObject(i);
                    }
                }
            }
            connection.close();
        } catch (SQLException e) {
            logger.error("sql err : ", e);
            throw new ChainClientException("sql err : " + e.getMessage());
        }

        try {
            return Store.BlockWithRWSet.parseFrom(blockWithRWSetBytes);
        } catch (InvalidProtocolBufferException e) {
            logger.error("blockWithRWSet parseFrom result : ", e);
            throw new ChainClientException("blockWithRWSet parseFrom result : " + e.getMessage());
        }
    }

    // ## 8 管理类接口
    // ### 8.1 SDK停止接口：关闭连接池连接，释放资源
    public void stop() throws ChainClientException {
        RpcServiceClient rpcServiceClient = connectionPool.getConnection();
        if (rpcServiceClient == null) {
            logger.error("all connections no Idle or Ready");
            throw new ChainClientException("all connections no Idle or Ready, please reSet connection count");
        }
        rpcServiceClient.getManagedChannel().shutdown();
    }

    private byte[] createPayloadOfQuery(String contractName, String method, Map<String, String> params) {
        // create request payload
        Request.QueryPayload.Builder queryPayloadBuilder = Request.QueryPayload.newBuilder()
                .setContractName(contractName)
                .setMethod(method == null ? "" : method);

        if (params != null) {
            params.forEach((key, value) -> queryPayloadBuilder.addParameters(
                    Request.KeyValuePair.newBuilder().setKey(key).setValue(value).build()));
        }

        return queryPayloadBuilder.build().toByteArray();
    }

    private byte[] createPayloadOfTransaction(String contractName, String method, Map<String, String> params) {
        // create request payload
        Request.TransactPayload.Builder queryPayloadBuilder = Request.TransactPayload.newBuilder()
                .setContractName(contractName)
                .setMethod(method == null ? "" : method);

        if (params != null && !params.isEmpty()) {
            params.forEach((key, value) -> queryPayloadBuilder.addParameters(
                    Request.KeyValuePair.newBuilder().setKey(key).setValue(value).build()));
        }

        return queryPayloadBuilder.build().toByteArray();
    }

    private byte[] createPayloadOfSubscribe(long startBlock, long endBlock, boolean withRWSet) {
        // create request payload
        Request.SubscribeBlockPayload.Builder subscribePayloadBuilder = Request.SubscribeBlockPayload.newBuilder()
                .setStartBlock(startBlock)
                .setEndBlock(endBlock)
                .setWithRwSet(withRWSet);

        return subscribePayloadBuilder.build().toByteArray();
    }

    private byte[] createPayloadOfSubscribeTx(long startBlock, long endBlock, Request.TxType txType, String[] txIds) {
        // create request payload
        Request.SubscribeTxPayload.Builder subscribeTxPayloadBuilder = Request.SubscribeTxPayload.newBuilder()
                .setStartBlock(startBlock)
                .setEndBlock(endBlock)
                .setTxType(txType)
                .addAllTxIds(Arrays.asList(txIds));

        return subscribeTxPayloadBuilder.build().toByteArray();
    }

    private byte[] createPayloadOfSubscribeContractEvent(String topic, String contractName) {
        // create request payload
        Request.SubscribeContractEventPayload.Builder subscribeContractEventPayloadBuilder = Request.SubscribeContractEventPayload.newBuilder()
                .setContractName(contractName)
                .setTopic(topic);

        return subscribeContractEventPayloadBuilder.build().toByteArray();
    }

    private byte[] createPayloadOfSubscribeHistoryContractEvent(int startBlock, int endBlock, String topic, String contractName) {
        // create request payload
        Request.SubscribeContractEventPayload.Builder subscribeContractEventPayloadBuilder = Request.SubscribeContractEventPayload.newBuilder()
                .setStartBlock(startBlock)
                .setEndBlock(endBlock)
                .setContractName(contractName)
                .setTopic(topic);

        return subscribeContractEventPayloadBuilder.build().toByteArray();
    }

    private ResponseInfo sendRequest(Request.TxType txType, byte[] payloadWithEndorsementsBytes, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        return sendTxRequest(createTxRequest(txType, payloadWithEndorsementsBytes), timeout);
    }

    private ResponseInfo sendRequest(Request.TxType txType, byte[] payloadWithEndorsementsBytes, String txId, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {
        return sendTxRequest(createTxRequest(txType, payloadWithEndorsementsBytes, txId), timeout);
    }

    private byte[] createPayloadOfSystemContract(String contractName, String method,
                                               long sequence, Map<String, String> params) {
        Request.SystemContractPayload.Builder systemContractPayloadBuilder = Request.SystemContractPayload.newBuilder()
                .setChainId(chainId).setContractName(contractName)
                .setMethod(method).setSequence(sequence);

        if (params != null && !params.isEmpty()) {
            params.forEach((key, value) -> systemContractPayloadBuilder.addParameters(
                    Request.KeyValuePair.newBuilder().setKey(key).setValue(value).build()));
        }

        return systemContractPayloadBuilder.build().toByteArray();
    }

    private byte[] createPayloadOfContractMgmt(String contractName, String contractVersion,
                                               ContractOuterClass.RuntimeType runtimeType, String manageType,
                                               Map<String, String> params, byte[] byteCodes) {
        ContractOuterClass.ContractId.Builder contractIdBuilder = ContractOuterClass.ContractId.newBuilder().setContractName(contractName);
        if (contractVersion != null) {
            contractIdBuilder.setContractVersion(contractVersion);
        }
        if (runtimeType != null) {
            contractIdBuilder.setRuntimeType(runtimeType);
        }
        Request.ContractMgmtPayload.Builder mgmtPayloadBuilder = Request.ContractMgmtPayload.newBuilder()
                .setChainId(chainId).setContractId(contractIdBuilder.build());
        if (byteCodes != null) {
            mgmtPayloadBuilder.setByteCode(ByteString.copyFrom(byteCodes));
        }
        if (manageType != null) {
            mgmtPayloadBuilder.setMethod(manageType);
        }
        if (params != null && !params.isEmpty()) {
            params.forEach((key, value) -> mgmtPayloadBuilder.addParameters(
                    Request.KeyValuePair.newBuilder().setKey(key).setValue(value).build()));
        }

        return mgmtPayloadBuilder.build().toByteArray();
    }

    private Request.TxRequest createTxRequest(Request.TxType txType, byte[] payloadWithEndorsementsBytes) throws ChainMakerCryptoSuiteException {
        String txId = Utils.generateTxId(ByteString.copyFrom(UUID.randomUUID().toString().getBytes())
                .concat(ByteString.copyFrom(UUID.randomUUID().toString().getBytes())), clientUser.getCryptoSuite());

        Request.TxHeader.Builder txHeaderBuilder = Request.TxHeader.newBuilder()
                .setTimestamp(Utils.getCurrentTimeSeconds())
                .setTxId(txId)
                .setChainId(chainId)
                .setSender(clientUser.getSerializedMember(isEnabledCertHash))
                .setTxType(txType)
                .setExpirationTime(0);
        Request.TxRequest.Builder txRequestBuilder = Request.TxRequest.newBuilder()
                .setPayload(ByteString.copyFrom(payloadWithEndorsementsBytes))
                .setHeader(txHeaderBuilder.build());

        return signRequest(txRequestBuilder);
    }

    private Request.TxRequest createTxRequest(Request.TxType txType, byte[] payloadWithEndorsementsBytes, String txId) throws ChainMakerCryptoSuiteException {
        Request.TxHeader.Builder txHeaderBuilder = Request.TxHeader.newBuilder()
                .setTimestamp(Utils.getCurrentTimeSeconds())
                .setTxId(txId)
                .setChainId(chainId)
                .setSender(clientUser.getSerializedMember(isEnabledCertHash))
                .setTxType(txType)
                .setExpirationTime(0);
        Request.TxRequest.Builder txRequestBuilder = Request.TxRequest.newBuilder()
                .setPayload(ByteString.copyFrom(payloadWithEndorsementsBytes))
                .setHeader(txHeaderBuilder.build());

        return signRequest(txRequestBuilder);
    }

    private Request.TxRequest signRequest(Request.TxRequest.Builder txRequestBuilder) throws ChainMakerCryptoSuiteException {

        byte[] headerBytes = txRequestBuilder.getHeader().toByteArray();
        byte[] payloadBytes = txRequestBuilder.getPayload().toByteArray();

        byte[] signBody = new byte[headerBytes.length + payloadBytes.length];
        System.arraycopy(headerBytes, 0, signBody, 0, headerBytes.length);
        System.arraycopy(payloadBytes, 0, signBody, headerBytes.length, payloadBytes.length);

        txRequestBuilder.setSignature(ByteString.copyFrom(clientUser.getCryptoSuite().sign(clientUser.getPrivateKey(), signBody)));

        return txRequestBuilder.build();
    }

    private ResponseInfo sendTxRequest(Request.TxRequest signedRequest, long timeout) throws ChainClientException {
        ResultOuterClass.TxResponse txResponse = null;
        RpcServiceClient rpcServiceClient = connectionPool.getConnection();
        try {
            txResponse = rpcServiceClient.getRpcNodeFutureStub().sendRequest(signedRequest)
                    .get(timeout, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            logger.error("connect to peer error : ", e);
            throw new ChainClientException("connect to peer error : " + e.getMessage());
        }
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setTxResponse(txResponse);
        responseInfo.setTxId(signedRequest.getHeader().getTxId());
        return responseInfo;
    }

    private byte[] getCertificateId(String hashType) throws ChainMakerCryptoSuiteException {
        if (clientUser.getCertBytes() == null || clientUser.getCertBytes().length == 0) {
            return new byte[0];
        }
        byte[] encodedCert;
        try {
            encodedCert = clientUser.getCertificate().getEncoded();
        } catch (CertificateEncodingException e) {
            logger.error("encoded cert err : ", e);
            throw new ChainMakerCryptoSuiteException("encoded cert err : " + e.getMessage());
        }
        if (encodedCert == null || encodedCert.length == 0) {
            return new byte[0];
        }

        Digest digest = getHashDigest(hashType);
        byte[] hash = new byte[digest.getDigestSize()];
        digest.update(encodedCert, 0, encodedCert.length);
        digest.doFinal(hash, 0);

        return hash;
    }

    private Digest getHashDigest(String hashAlgorithm) {
        if ("SHA3".equals(hashAlgorithm)) {
            return new SHA3Digest();
        } else if ("SM3".equals(hashAlgorithm)) {
            return new SM3Digest();
        } else {
            // Default to SHA2
            return new SHA256Digest();
        }
    }

    private boolean checkCertHashOnChain() throws ChainClientException, ChainMakerCryptoSuiteException {
        byte[] certHash = clientUser.getCertHash();
        if (certHash == null || certHash.length == 0) {
            return false;
        }

        ResultOuterClass.CertInfos certInfos = queryCert(new String[]{ByteUtils.toHexString(certHash)}, DEFAULT_RPC_TIMEOUT);
        if (certInfos == null) {
            throw new ChainClientException("get cert infos failed");
        }
        if (certInfos.getCertInfosList().size() != 1 || certInfos.getCertInfos(0).getCert().isEmpty()) {
            return false;
        }
        return certInfos.getCertInfos(0).getHash().equals(ByteUtils.toHexString(certHash));
    }

    private ChainmakerTransaction.TransactionInfo loopQueryResultByTxId(String txId, long timeout)
            throws ChainMakerCryptoSuiteException, ChainClientException {

        // get try cont from timeout, will sleep 2 seconds between queries
        long tryCount = timeout%2000==0?timeout/2000:timeout/2000+1;
        if (tryCount == 0) {
            tryCount++;
        }
        for (long i = 0; i < tryCount; i++) {
            ChainmakerTransaction.TransactionInfo transactionInfo = getTxByTxId(txId, timeout);
            if (transactionInfo == null || !transactionInfo.hasTransaction()) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                continue;
            }
            return transactionInfo;
        }
        return null;
    }

    private long getBlockHeight(String txId, String blockHash, long timeout) throws ChainMakerCryptoSuiteException, ChainClientException {

        Map<String, String> params = new HashMap<>();
        String method = "";

        if (!txId.equals("")) {
            method = ContractOuterClass.QueryFunction.GET_BLOCK_HEIGHT_BY_TX_ID.toString();
            params.put(TX_ID, txId);
        } else if (!blockHash.equals("")) {
            method = ContractOuterClass.QueryFunction.GET_BLOCK_HEIGHT_BY_HASH.toString();
            params.put(BLOCK_HASH, blockHash);
        }
        byte[] payloadOfQuery = createPayloadOfQuery(ContractOuterClass.ContractName.SYSTEM_CONTRACT_QUERY.toString(),
                method, params);

        ResponseInfo responseInfo = sendRequest(Request.TxType.QUERY_SYSTEM_CONTRACT, payloadOfQuery, timeout);
        if (responseInfo.getTxResponse() == null) {
            return -1;
        }

        return Integer.parseInt(responseInfo.getTxResponse().getContractResult().getResult().toStringUtf8());
    }
}
