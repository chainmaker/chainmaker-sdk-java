/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

/*
 Node is the destination which a ChainClient connect to. It include the grpc address, hostname,
 negotiationType and client info(cert and key).
 */
public class Node {
    // node grpc address
    private String grpcUrl;
    // rpc client private key bytes
    private byte[] clientKeyBytes;
    // rpc client certificate bytes
    private byte[] clientCertBytes;
    // the organization's ca cert bytes
    private byte[][] tlsCertBytes;
    // the hostname in client certificate
    private String hostname;
    // TLS or PLAINTEXT
    private String negotiationType;
    // OPENSSL or JDK
    private String sslProvider;

    private int connectCount = 10;

    private int messageSize  = 16;

    public String getGrpcUrl() {
        return grpcUrl;
    }

    public void setGrpcUrl(String grpcUrl) {
        this.grpcUrl = grpcUrl;
    }

    public byte[] getClientKeyBytes() {
        return clientKeyBytes;
    }

    public void setClientKeyBytes(byte[] clientKeyBytes) {
        this.clientKeyBytes = clientKeyBytes;
    }

    public byte[] getClientCertBytes() {
        return clientCertBytes;
    }

    public void setClientCertBytes(byte[] clientCertBytes) {
        this.clientCertBytes = clientCertBytes;
    }

    public byte[][] getTlsCertBytes() {
        return tlsCertBytes;
    }

    public void setTlsCertBytes(byte[][] tlsCertBytes) {
        this.tlsCertBytes = tlsCertBytes;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getNegotiationType() {
        return negotiationType;
    }

    public void setNegotiationType(String negotiationType) {
        this.negotiationType = negotiationType;
    }

    public String getSslProvider() {
        return sslProvider;
    }

    public void setSslProvider(String sslProvider) {
        this.sslProvider = sslProvider;
    }

    public int getConnectCount() {
        return connectCount;
    }

    public void setConnectCount(int connectCount) {
        this.connectCount = connectCount;
    }

    public int getMessageSize() {
        return messageSize;
    }

    public void setMessageSize(int messageSize) {
        this.messageSize = messageSize;
    }
}
