/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.chainmaker.sdk.config.ArchiveConfig;
import org.chainmaker.sdk.utils.UtilsException;

public class ChainManager {
    // chains' map
    private final Map<String, ChainClient> chains = new HashMap<>();
    // for singleton mode
    private static final ChainManager chainManager = new ChainManager();

    private ChainManager() {
    }

    // singleton getInstance
    public static ChainManager getInstance() {
        return chainManager;
    }

    // get a chain client from chains
    public ChainClient getChainClient(String chainId) {
        return chains.get(chainId);
    }

    // create a chain client by chain id, client user and nodes
    public synchronized ChainClient createChainClient(String chainId, User clientUser, Node[] nodes)
            throws ChainClientException, RpcServiceClientException, UtilsException {
        ChainClient chainClient = chains.get(chainId);
        if (chainClient != null) {
            return chainClient;
        }
        List<RpcServiceClient> rpcServiceClients = new ArrayList<>();
        for(Node node : nodes) {
            for (int i = 0; i < node.getConnectCount(); i++) {
                RpcServiceClient rpcServiceClient = RpcServiceClient.newServiceClient(node);
                rpcServiceClients.add(rpcServiceClient);
            }
        }

        Collections.shuffle(rpcServiceClients);

        ConnectionPool connectionPool = new ConnectionPool();
        connectionPool.setRpcServiceClients(rpcServiceClients);

        chainClient = new ChainClient();
        chainClient.setChainId(chainId);
        chainClient.setNodes(new ArrayList<>());
        chainClient.setClientUser(clientUser);
        chainClient.setConnectionPool(connectionPool);

        chains.put(chainId, chainClient);

        return chainClient;
    }

    public synchronized ChainClient createChainClient(String chainId, User clientUser, Node[] nodes, ArchiveConfig archiveConfig)
            throws ChainClientException, RpcServiceClientException, UtilsException {
        ChainClient chainClient = createChainClient(chainId, clientUser, nodes);
        chainClient.setArchiveConfig(archiveConfig);
        return chainClient;
    }
}
