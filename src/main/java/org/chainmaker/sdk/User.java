/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import org.chainmaker.pb.accesscontrol.Member;
import org.chainmaker.pb.common.Request;
import org.chainmaker.sdk.crypto.ChainMakerCryptoSuiteException;
import org.chainmaker.sdk.crypto.ChainmakerX509CryptoSuite;
import org.chainmaker.sdk.crypto.CryptoSuite;
import org.chainmaker.sdk.utils.CryptoUtils;

import java.security.PrivateKey;
import java.security.cert.Certificate;

/*
 User means a people who use the chains. Usually a user has a private key, a cert and an organization,
 so you can use a user to sign a transaction.
 */
public class User {

    // the organization id of the user
    private String orgId;
    // user's private key used to sign transaction
    private PrivateKey privateKey;
    // user's certificate
    private Certificate certificate;
    // the bytes of user's certificate
    private final byte[] certBytes;
    // the hash of the cert
    private byte[] certHash;

    private CryptoSuite cryptoSuite;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public byte[] getCertBytes() {
        return certBytes;
    }

    public byte[] getCertHash() {
        return certHash;
    }

    public void setCertHash(byte[] certHash) {
        this.certHash = certHash;
    }

    public CryptoSuite getCryptoSuite() {
        return cryptoSuite;
    }

    public void setCryptoSuite(CryptoSuite cryptoSuite) {
        this.cryptoSuite = cryptoSuite;
    }

    // Construct a user by organization id, user's private key bytes and user's cert bytes
    public User(String orgId, byte[] userKeyBytes, byte[] userCertBytes) throws ChainMakerCryptoSuiteException {
        PrivateKey generatedPrivateKey = CryptoUtils.getPrivateKeyFromBytes(userKeyBytes);
        CryptoSuite generatedCryptoSuite = ChainmakerX509CryptoSuite.newInstance();
        Certificate generatedCertificate = generatedCryptoSuite.getCertificateFromBytes(userCertBytes);
        this.orgId = orgId;
        this.certBytes = userCertBytes;
        this.privateKey = generatedPrivateKey;
        this.cryptoSuite = generatedCryptoSuite;
        this.certificate = generatedCertificate;
    }

    // Sign the payload of contract management
    public byte[] signPayloadOfContractMgmt(byte[] payload, boolean isEnabledCertHash)
            throws ChainMakerCryptoSuiteException {
        Request.ContractMgmtPayload contractMgmtPayload;
        Request.ContractMgmtPayload.Builder contractMgmtPayloadBuilder;
        try {
            contractMgmtPayload = Request.ContractMgmtPayload.parseFrom(payload);
        } catch (InvalidProtocolBufferException e) {
            throw new ChainMakerCryptoSuiteException("contractMgmtPayload parseFrom result : " + e.getMessage());
        }
        contractMgmtPayloadBuilder = contractMgmtPayload.toBuilder();
        Request.EndorsementEntry endorsementEntry = Request.EndorsementEntry.newBuilder().setSignature(
                ByteString.copyFrom(cryptoSuite.sign(privateKey, payload))).
                setSigner(getSerializedMember(isEnabledCertHash)).build();
        contractMgmtPayloadBuilder.addEndorsement(endorsementEntry);
        return contractMgmtPayloadBuilder.build().toByteArray();
    }

    // Sign the payload of system contract
    public byte[] signPayloadOfSystemContract(byte[] payloadBytes, boolean isEnabledCertHash)
            throws ChainMakerCryptoSuiteException {
        Request.SystemContractPayload systemContractPayload;
        Request.SystemContractPayload.Builder configUpdatePayloadBuilder;
        try {
            systemContractPayload = Request.SystemContractPayload.parseFrom(payloadBytes);
        } catch (InvalidProtocolBufferException e) {
            throw new ChainMakerCryptoSuiteException("SystemContractPayload parseFrom result : " + e.getMessage());
        }
        configUpdatePayloadBuilder = systemContractPayload.toBuilder();
        Request.EndorsementEntry endorsementEntry = Request.EndorsementEntry.newBuilder().setSignature(
                ByteString.copyFrom(cryptoSuite.sign(privateKey, payloadBytes)))
                .setSigner(getSerializedMember(isEnabledCertHash)).build();
        configUpdatePayloadBuilder.addEndorsement(endorsementEntry);
        return configUpdatePayloadBuilder.build().toByteArray();
    }

    // Sign the payload of multi sign request and return the endorsement
    public Request.EndorsementEntry signPayloadOfMultiSign(byte[] payload, boolean isEnabledCertHash)
            throws ChainMakerCryptoSuiteException {
        return Request.EndorsementEntry.newBuilder().setSignature(
                ByteString.copyFrom(cryptoSuite.sign(privateKey, payload))).
                setSigner(getSerializedMember(isEnabledCertHash)).build();
    }

    // Get the SerializedMember according whether enabled cert hash
    public Member.SerializedMember getSerializedMember(boolean isEnabledCertHash) {
        if (isEnabledCertHash && certHash != null && certHash.length > 0) {
            return Member.SerializedMember.newBuilder()
                    .setOrgId(orgId)
                    .setMemberInfo(ByteString.copyFrom(certHash))
                    .setIsFullCert(false)
                    .build();
        }
        return Member.SerializedMember.newBuilder()
                .setOrgId(orgId)
                .setMemberInfo(ByteString.copyFrom(certBytes))
                .setIsFullCert(true)
                .build();
    }
}
