# 适配版本一览表

| chainmaker-go | chainmaker-sdk-java | 
| :----------------| :----------------| 
| v1.0.0           | v1.0.0           | 
| v1.1.0           | v1.1.0           | 
| v1.1.1           | v1.1.0           | 
| v1.2.0           | v1.2.0           | 
| v1.2.3           | v1.2.3           | 
| v1.2.4           | v1.2.4           | 
| v1.2.6           | v1.2.6           | 


2.x版本见： https://git.chainmaker.org.cn/chainmaker/sdk-java
