# ChainMaker java SDK 接口说明
## 1 用户合约接口
### 1.1 生成用于创建合约的待签名payload
**参数说明**

  - contractName: 合约名
  - version: 版本号
  - runtimeType: 合约运行环境
  - params: 合约初始化参数
  - byteCodes: 合约字节数组

**返回值说明**
    返回payload字节数组

```java
    public byte[] createPayloadOfContractCreation(String contractName, String version, 
                                                  Contract.RuntimeType runtimeType, Map<String, String> params, 
                                                  byte[] byteCodes) {
    }
```

### 1.2 生成用于升级合约的待签名payload
 **参数说明**

  - contractName: 合约名
  - version: 版本号
  - runtimeType: 合约运行环境
  - params: 合约初始化参数
  - byteCodes: 合约字节数组

**返回值说明**
    返回payload字节数组

```java
public byte[] createPayloadOfContractUpgrade(String contractName, String version,
                                             Contract.RuntimeType runtimeType, Map<String, String> params,
                                             byte[] byteCodes) {
```
### 1.3 生成用于冻结合约的待签名payload
 **参数说明**

  - contractName: 合约名
**返回值说明**
    返回payload字节数组
```java
public byte[] createPayloadOfContractFreeze(String contractName) {}
```
#### 1.4 生成用于解冻合约的待签名payload
 **参数说明**

  - contractName: 合约名
**返回值说明**
    返回payload字节数组
```java
public byte[] createPayloadOfContractUnfreeze(String contractName) {}
```
### 1.5 生成用于吊销合约的待签名payload
 **参数说明**

  - contractName: 合约名
**返回值说明**
    返回payload字节数组
```java
public byte[] createPayloadOfContractRevoke(String contractName) {}
```

### 1.6 将多个带签名后的payload合并成一个带多个签名的payload，所有签名都放在一个payload后面

**参数说明**
  - payloads: 多个带签名的payload

**返回值说明**
    返回带多个签名的payload字节数组
```java
    public byte[] mergeSignedPayloadsOfContractMgmt(byte[][] payloads) throws InvalidProtocolBufferException {
    }
```

### 1.7 创建合约
**参数说明**
  - payloadWithEndorsementsBytes: 带签名的合约内容
  - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
  - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，直接返回（返回信息里包含交易ID），单位：毫秒
```java
    public ResponseInfo createContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                       long syncResultTimeout) throws InvalidProtocolBufferException, 
																				ChainMakerCryptoSuiteException {
    }
```

### 1.8 升级合约
**参数说明**

  - payloadWithEndorsementsBytes: 带签名的合约内容
  - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
  - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，
    直接返回（返回信息里包含交易ID），单位：毫秒
```java
    public ResponseInfo upgradeContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                        long syncResultTimeout) throws InvalidProtocolBufferException, 
																				ChainMakerCryptoSuiteException {
    }
```
### 1.9 冻结合约
**参数说明**

  - payloadWithEndorsementsBytes: 带签名的合约内容
  - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
  - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，
    直接返回（返回信息里包含交易ID），单位：毫秒
```java
    public ResponseInfo freezeContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                        long syncResultTimeout) throws InvalidProtocolBufferException, 
																				ChainMakerCryptoSuiteException {}
```
### 1.10 解冻合约
**参数说明**

  - payloadWithEndorsementsBytes: 带签名的合约内容
  - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
  - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，
    直接返回（返回信息里包含交易ID），单位：毫秒
```java
    public ResponseInfo unfreezeContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                       long syncResultTimeout) throws InvalidProtocolBufferException, 
																				ChainMakerCryptoSuiteException {}
```
### 1.11 吊销合约
**参数说明**

  - payloadWithEndorsementsBytes: 带签名的合约内容
  - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
  - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，
    直接返回（返回信息里包含交易ID），单位：毫秒
```java
    public ResponseInfo revokeContract(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout,
                                       long syncResultTimeout) throws InvalidProtocolBufferException, 
																				ChainMakerCryptoSuiteException {
```

### 1.12 执行合约

**参数说明**
  - contractName: 合约名
  - method: 方法名
  - params: 执行参数
  - rpcCallTimeout: 调用rcp接口超时时间, 单位：毫秒
  - syncResultTimeout: 同步获取执行结果超时时间，小于等于0代表不等待执行结果，直接返回（返回信息里包含交易ID），单位：毫秒
```java
    public ResponseInfo invokeContract(String contractName, String method,
                                       Map<String, String> params, long rpcCallTimeout,
                                       long syncResultTimeout) throws InvalidProtocolBufferException, 
																				ChainMakerCryptoSuiteException {
    }
```

### 1.13 查询合约接口
**参数说明**
  - contractName: 合约名
  - method: 方法名
  - params: 执行参数
  - timeout: 执行超时时间，单位毫秒
```java
    public ResponseInfo queryContract(String contractName, String method,
                                      Map<String, String> params, long timeout) throws ChainMakerCryptoSuiteException {
    }
```


## 2 系统合约接口
### 2.1 根据交易Id查询交易
**参数说明**
  - txId: 交易ID
  - timeout：超时时间，单位毫秒
```java
    public ChainmakerTransaction.TransactionInfo getTxByTxId(String txId, long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException  {
    }
```

### 2.2 根据区块高度查询区块
**参数说明**
  - blockHeight: 区块高度
  - withRWSet: 是否返回读写集
  - timeout：超时时间，单位毫秒
```java
    public ChainmakerBlock.BlockInfo getBlockByHeight(long blockHeight, boolean withRWSet, long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 2.3 根据区块哈希查询区块
**参数说明**
  - blockHash: 区块高度
  - withRWSet: 是否返回读写集
  - timeout：超时时间，单位毫秒
```java
    public ChainmakerBlock.BlockInfo getBlockByHash(String blockHash, boolean withRWSet, long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 2.4 根据交易Id查询区块
**参数说明**
  - txId: 交易Id
  - withRWSet: 是否返回读写集
  - timeout：超时时间，单位毫秒
```java
    public ChainmakerBlock.BlockInfo getBlockByTxId(String txId, boolean withRWSet, long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 2.5 查询上一个配置块
**参数说明**
  - withRWSet: 是否返回读写集
  - timeout：超时时间，单位毫秒
```java
    public ChainmakerBlock.BlockInfo getLastConfigBlock(boolean withRWSet, long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 2.6 查询节点加入的链信息

**参数说明**

   - timeout：超时时间，单位毫秒

**返回值说明**

-  返回ChainId清单

```java
    public Discovery.ChainList getNodeChainList(long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 2.7 查询链信息

**返回值说明**

  - timeout：超时时间，单位毫秒
```java
    public Discovery.ChainInfo getChainInfo(long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 2.8 根据txId查询区块高度

**返回值说明**

  - txId: 交易id
  - timeout：超时时间，单位毫秒
```java
    public long getBlockHeightByTxId(String txId, long timeout) 
            throws ChainMakerCryptoSuiteException, ChainClientException {
    }
```

### 2.9 根据blockHash查询区块高度

**返回值说明**

  - 区块哈希: blockHash
  - timeout：超时时间，单位毫秒
```java
    public getBlockHeightByBlockHash(String blockHash, long timeout) 
            throws ChainMakerCryptoSuiteException, ChainClientException {
    }
```

### 2.10 根据区块高度查询完整区块

**返回值说明**

  - 区块高度: blockHeight
  - timeout：超时时间，单位毫秒
```java
    public Store.BlockWithRWSet getFullBlockByHeight(long blockHeight, long timeout) 
            throws ChainClientException, ChainMakerCryptoSuiteException, InvalidProtocolBufferException {
    }
```

### 2.11 查询最新区块信息

**返回值说明**

  - 区块高度: blockHeight
  - timeout：超时时间，单位毫秒
```java
    public ChainmakerBlock.BlockInfo getLastBlcok(boolean withRWSet, long timeout) 
            throws InvalidProtocolBufferException, ChainClientException, ChainMakerCryptoSuiteException {
    }
```

### 2.12 查询最新区块高度

**返回值说明**

  - timeout：超时时间，单位毫秒
```java
    public long getCurrentBlockHeight(long timeout) 
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException, ChainClientException {
    }
```

### 2.13 根据区块高度查询区块头

**返回值说明**

  - timeout：超时时间，单位毫秒
```java
    public ChainmakerBlock.BlockHeader getBlockHeaderByHeight(long blockHeight, long timeout) 
            throws ChainClientException, ChainMakerCryptoSuiteException, InvalidProtocolBufferException {
    }
```


##3 链配置接口
### 3.1 查询最新链配置
```java
    public ChainmakerConfig.ChainConfig getChainConfig(long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.2 根据指定区块高度查询最近链配置
  - 如果当前区块就是配置块，直接返回当前区块的链配置
```java
    public ChainmakerConfig.ChainConfig getChainConfigByBlockHeight(int blockHeight, long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.3 查询最新链配置序号Sequence
  - 用于链配置更新
```java
    public long getChainConfigSequence(long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.4 更新Core模块待签名payload生成
**参数说明**
  - txSchedulerTimeout: 交易调度器从交易池拿到交易后, 进行调度的时间，其值范围为[0, 60]，若无需修改，请置为-1
  - txSchedulerValidateTimeout: 交易调度器从区块中拿到交易后, 进行验证的超时时间，其值范围为[0, 60]，
    若无需修改，请置为-1
```java
    public byte[] createPayloadOfChainConfigCoreUpdate(int txSchedulerTimeout, int txSchedulerValidateTimeout,
    																									 long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainClientException, ChainMakerCryptoSuiteException {
    }
```

### 3.5 更新Core模块待签名payload生成
**参数说明**

  - txTimestampVerify: 是否需要开启交易时间戳校验
  - (以下参数，若无需修改，请置为-1)
  - txTimeout: 交易时间戳的过期时间(秒)，其值范围为[600, +∞)
  - blockTxCapacity: 区块中最大交易数，其值范围为(0, +∞]
  - blockSize: 区块最大限制，单位MB，其值范围为(0, +∞]
  - blockInterval: 出块间隔，单位:ms，其值范围为[10, +∞]
```java
    public byte[] createPayloadOfChainConfigBlockUpdate(boolean txTimestampVerify, int txTimeout, 
    																										int blockTxCapacity, int blockSize, int blockInterval,
                                                        long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainClientException, ChainMakerCryptoSuiteException {
    }
```

### 3.6 添加信任组织根证书待签名payload生成
**参数说明**
  - trustRootOrgId: 组织Id
  - trustRootCrt: 根证书
```java
    public byte[] createPayloadOfChainConfigTrustRootAdd(String trustRootOrgId, String trustRootCrt, 
    																										 long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.7 更新信任组织根证书待签名payload生成
**参数说明**
  - trustRootOrgId: 组织Id
  - trustRootCrt: 根证书
```java
    public byte[] createPayloadOfChainConfigTrustRootUpdate(String trustRootOrgId, String trustRootCrt, 
    																												long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.8 删除信任组织根证书待签名payload生成
**参数说明**
  - trustRootOrgId: 组织Id
```java
    public byte[] createPayloadOfChainConfigTrustRootDelete(String trustRootOrgId, long timeout)
            throws TimeoutException, InvalidProtocolBufferException {
    }
```

### 3.9 添加权限配置待签名payload生成
**参数说明**
  - permissionResourceName: 权限名
  - principle: 权限规则
```java
    public byte[] createPayloadOfChainConfigPermissionAdd(String permissionResourceName,
                                                          PrincipleOuterClass.Principle principal, long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.10 更新权限配置待签名payload生成
**参数说明**
  - permissionResourceName: 权限名
  - principle: 权限规则
```java
    public byte[] createPayloadOfChainConfigPermissionUpdate(String permissionResourceName,
                                                             PrincipleOuterClass.Principle principal, 
                                                             long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.11 删除权限配置待签名payload生成
**参数说明**
  - permissionResourceName: 权限名
```java
    public byte[] createPayloadOfChainConfigPermissionDelete(String permissionResourceName, long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.12 添加共识节点地址待签名payload生成
**参数说明**
  - nodeOrgId: 节点组织Id
  - nodeAddresses: 节点地址
```java
    public byte[] createPayloadOfChainConfigConsensusNodeAddrAdd(String nodeOrgId, String[] nodeAddresses, 
    																														 long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.13 更新共识节点地址待签名payload生成
**参数说明**
  - nodeOrgId: 节点组织Id
  - nodeOldAddress: 节点原地址
  - nodeNewAddress: 节点新地址
```java
    public byte[] createPayloadOfChainConfigConsensusNodeAddrUpdate(String nodeOrgId, String nodeOldAddress,
                                                                    String nodeNewAddress, long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.14 删除共识节点地址待签名payload生成
**参数说明**
  - nodeOrgId: 节点组织Id
  - nodeAddress: 节点地址
```java
    public byte[] createPayloadOfChainConfigConsensusNodeAddrDelete(String nodeOrgId, String nodeAddress, 
    																																long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.15 添加共识节点待签名payload生成
**参数说明**
  - nodeOrgId: 节点组织Id
  - nodeAddresses: 节点地址
```java
    public byte[] createPayloadOfChainConfigConsensusNodeOrgAdd(String nodeOrgId, String[] nodeAddresses, 
    																														long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.16 更新共识节点待签名payload生成
**参数说明**

  - nodeOrgId: 节点组织Id
  - nodeAddresses: 节点地址
```java
    public byte[] createPayloadOfChainConfigConsensusNodeOrgUpdate(String nodeOrgId, String[] nodeAddresses, 
    																															 long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.17 删除共识节点待签名payload生成
**参数说明**
  - nodeOrgId: 节点组织Id
```java
    public byte[] createChainConfigConsensusNodeOrgDeletePayload(String nodeOrgId, long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.18 添加共识扩展字段待签名payload生成
**参数说明**
  - params: Map<String, String>
```java
    public byte[] createPayloadOfChainConfigConsensusExtAdd(Map<String, String> params, long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.19 添加共识扩展字段待签名payload生成
**参数说明**
  - params: Map<String, String>
```java
    public byte[] createPayloadOfChainConfigConsensusExtUpdatePayload(Map<String, String> params, long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.20 添加共识扩展字段待签名payload生成
**参数说明**
  - keys: 待删除字段
```java
    public byte[] createPayloadOfChainConfigConsensusExtDelete(String[] keys, long timeout)
            throws TimeoutException, InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 3.21 链配置更新Payload签名收集&合并
```java
    public byte[] mergeSignedPayloadsOfChainConfig(byte[][] payloads) throws InvalidProtocolBufferException {
    }
```

### 3.22 发送链配置更新请求
```java
    public ResponseInfo updateChainConfig(byte[] payloadWithEndorsementsBytes, long rpcCallTimeout) throws ChainMakerCryptoSuiteException {
    }
```

## 4 证书管理接口
### 4.1 用户证书添加
**参数说明**
  - rpcCallTimeout: 发送rpc请求的超时时间
  - syncResultTimeout: 同步交易结果的超时时间
```java
    public ResponseInfo addCert(long rpcCallTimeout, long syncResultTimeout) 
      throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 4.2 用户证书删除
**参数说明**

  - certHashes: 证书Hash列表
```java
    public ResponseInfo deleteCert(String[] certHashes, long rpcCallTimeout, long syncResultTimeout) 
      throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 4.3 生成用户证书冻结的待签名payload
**参数说明**
  - certHashes: 冻结的证书内容列表
```java
   public byte[] createPayloadOfFreezeCerts(String[] certs) {}
```
### 4.4 生成用户证书解冻的待签名payload
**参数说明**
  - certHashes: 解冻的证书内容列表
```java
   public byte[] createPayloadOfFreezeCerts(String[] certs) {}
```
### 4.5 生成用户证书吊销的待签名payload
**参数说明**
  - certCrl: 吊销证书列表
```java
public byte[] createPayloadOfRevokeCerts(String certCrl) {}
```
### 4.6 冻结证书
**参数说明**
  - payload: 冻结证书交易的payload
  - rpcCallTimeout: 发送rpc请求的超时时间
  - syncResultTimeout: 同步交易结果的超时时间
```java
    public ResponseInfo freezeCerts(byte[] payload, long rpcCallTimeout, long syncResultTimeout) 
      throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {}
```
### 4.7 解冻证书
**参数说明**
  - payload: 解冻证书交易的payload
  - rpcCallTimeout: 发送rpc请求的超时时间
  - syncResultTimeout: 同步交易结果的超时时间
```java
    public ResponseInfo unfreezeCerts(byte[] payload, long rpcCallTimeout, long syncResultTimeout) 
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {}
```
### 4.8 吊销证书
**参数说明**
  - payload: 解冻证书交易的payload
  - rpcCallTimeout: 发送rpc请求的超时时间
  - syncResultTimeout: 同步交易结果的超时时间
```java
    public ResponseInfo revokeCerts(byte[] payload, long rpcCallTimeout, long syncResultTimeout) 
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {}
```
### 4.9 用户证书查询
**参数说明**
  - certHashes: 证书Hash列表

**返回值说明**
  - *pb.CertInfos: 包含证书Hash和证书内容的列表
```java
    public ChainmakerResult.CertInfos queryCert(String[] certHashes, long timeout)
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

## 5 多签接口
### 5.1 发送多签交易请求
**参数说明**
  - txType: 交易类型
  - payload: 多签交易（未签名）的payload
  - endorsement: 签名
  - deadlineBlock: 交易有效截止的block高度
  - rpcCallTimeout: 发送rpc请求的超时时间
  - syncResultTimeout: 同步交易结果的超时时间
```java
public ResponseInfo sendMultiSignRequest(Request.TxType txType, byte[] payload, 
                                         Request.EndorsementEntry endorsement,                                      
                                         long deadlineBlock, long rpcCallTimeout, 
                                         long syncResultTimeout) 
  throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {}
```
### 5.2 发送多签交易投票
**参数说明**
  - voteStatus: 投票状态
  - multiSignReqTxId: 投票交易的交易Id
  - payloadHash: 要投票交易的payloadHash
  - endorsement: 签名
  - deadlineBlock: 交易有效截止的block高度
  - rpcCallTimeout: 发送rpc请求的超时时间
  - syncResultTimeout: 同步交易结果的超时时间
```java
public ResponseInfo sendMultiSignVote(MultSign.VoteStatus voteStatus, String multiSignReqTxId, 
                                      String payloadHash,
                                      Request.EndorsementEntry endorsement, long rpcCallTimeout,
                                      long syncResultTimeout) 
  throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {}
```
## 6 消息订阅接口
### 6.1 区块订阅
**参数说明**
  - startBlock: 订阅起始区块高度，若为-1，表示订阅实时最新区块
  - endBlock: 订阅结束区块高度，若为-1，表示订阅实时最新区块
  - withRwSet: 是否返回读写集
```java
    public void subscribeBlock(long startBlock, long endBlock, boolean withRwSet,
                                StreamObserver<ChainmakerResult.SubscribeResult> blockStreamObserver) {
    }
```

### 6.2 交易订阅
**参数说明**
  - startBlock: 订阅起始区块高度，若为-1，表示订阅实时最新区块
  - endBlock: 订阅结束区块高度，若为-1，表示订阅实时最新区块
  - txType: 订阅交易类型,若为pb.TxType(-1)，表示订阅所有交易类型
  - txIds: 订阅txId列表，若为空，表示订阅所有txId
```java
    public void subscribeTx(long startBlock, long endBlock, Request.TxType txType, String[] txIds,
                            StreamObserver<ChainmakerResult.SubscribeResult> txStreamObserver) {
    }
```

### 6.3 事件订阅
**参数说明**
  - topic: 订阅话题
  - contractName: 订阅合约名
  - startBlock: 订阅起始区块高度，若为-1，表示订阅实时最新区块
  - endBlock: 订阅结束区块高度，若为-1，表示订阅实时最新区块
```java
    public void subscribeContractEvent(int startBlock, int endBlock, String topic, String contractName,
                                           StreamObserver<ResultOuterClass.SubscribeResult> contractEventStreamObserver)
```

## 7 归档类接口
### 7.1 数据归档payload生成
**参数说明**
  - targetBlockHeight: 归档区块高度
```java
    public byte[] createArchiveBlockPayload(long targetBlockHeight) {
    }
```

### 7.2 归档恢复payload生成
**参数说明**
  - fullBlock: 归档恢复数据
```java
    public byte[] createRestoreBlockPayload(byte[] fullBlock) {
    
    }
```

### 7.3 发送数据归档请求
**参数说明**
  - payloadBytes: 数据归档payload
```java
    public ResponseInfo sendArchiveBlockRequest(byte[] payloadBytes, long timeout) 
            throws ChainMakerCryptoSuiteException, ChainClientException {
    }
```

### 7.4 发送归档恢复请求
**参数说明**
  - payloadBytes: 归档恢复payload
```java
    public ResponseInfo sendRestoreBlockRequest(byte[] payloadBytes, long timeout) 
            throws ChainMakerCryptoSuiteException, ChainClientException {
    }
```

### 7.5 获取归档数据
**参数说明**
  - targetBlockHeight: 归档区块高度
```java
    public Store.BlockWithRWSet getArchivedFullBlockByHeight(long blockHeight) 
            throws InvalidProtocolBufferException, SQLException, ClassNotFoundException {
    }
```

### 7.6 获取归档区块信息
**参数说明**
  - targetBlockHeight: 归档区块高度
  - withRWSet: 是否获取读写集
```java
    public ChainmakerBlock.BlockInfo getArchivedBlockByHeight(long blockHeight, boolean withRWSet) 
            throws InvalidProtocolBufferException, SQLException, ClassNotFoundException {
    }
```

## 8 管理类接口
### 8.1 SDK停止接口：关闭连接池连接，释放资源
```java
public void stop() {}
```

## 9 User类接口

### 9.1 生成用于管理合约交易的签名后的payload

**参数说明**
  - payload: 签名前的payload

**返回值说明**
  - 返回带签名的payload字节数组

```java
    public byte[] signPayloadOfContractMgmt(byte[] payload, boolean isEnabledCertHash) 
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```

### 9.2 生成用于系统合约调用的签名后的payload

**参数说明**
  - payload: 签名前的payload

**返回值说明**
  - 返回带签名的payload字节数组

```java
    public byte[] signPayloadOfSystemContract(byte[] payloadBytes, boolean isEnabledCertHash) 
            throws InvalidProtocolBufferException, ChainMakerCryptoSuiteException {
    }
```
### 9.3 生成用于多签交易的payload

**参数说明**
  - payload: 签名前的payload

**返回值说明**
  - 返回包含签名的EndorsementEntry

```java
public Request.EndorsementEntry signPayloadOfMultiSign(byte[] payload, boolean isEnabledCertHash) throws ChainMakerCryptoSuiteException {}
```